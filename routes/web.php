<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::prefix('admin')->middleware('auth')->group(function () {
    //User
    Route::get('quan-tri','AdminController@index')->name('admin.index');
    Route::get('list-user','UserController@index')->name('user.index')->middleware(['can:view-user']); //danh sách user
    Route::get('create-user','UserController@create')->name('user.create')->middleware(['can:create-user']);  //tạo mới user
    Route::post('store-user','UserController@store')->name('user.store');
    Route::get('edit-user/{id}','UserController@edit')->name('user.edit')->middleware(['can:edit-user']); //Sửa user
    Route::post('update-user/{id}','UserController@update')->name('user.update');
    Route::delete('delete-user/{id}','UserController@destroy')->name('user.destroy')->middleware(['can:delete-user']); //Xóa user
   // Recruitment
    Route::get('list-recruitment','RecruitmentController@index')->name('recruitment.index')->middleware(['can:view-recruitment']);
    Route::get('create-recruitment','RecruitmentController@create')->name('recruitment.create')->middleware(['can:create-recruitment']);
    Route::post('store-recruitment','RecruitmentController@store')->name('recruitment.store');
    Route::get('edit-recruitment/{id}','RecruitmentController@edit')->name('recruitment.edit')->middleware(['can:edit-recruitment']);
    Route::post('update-recruitment/{id}','RecruitmentController@update')->name('recruitment.update');
    Route::delete('delete-recruitment/{id}','RecruitmentController@destroy')->name('recruitment.destroy')->middleware(['can:delete-recruitment']);//danh sách recruitment
    // Candidate
    Route::get('list-candidate','CandidateController@index')->name('candidate.index')->middleware(['can:view-candidate']);
//    Route::get('/admin/candidate/{id}', 'CandidateController@show')->name('candidate.show');
    Route::get('create-candidate','CandidateController@create')->name('candidate.create')->middleware(['can:create-candidate']);
    Route::post('store-candidate','CandidateController@store')->name('candidate.store');
    Route::get('edit-candidate/{id}','CandidateController@edit')->name('candidate.edit')->middleware(['can:edit-candidate']);
    Route::post('update-candidate/{id}','CandidateController@update')->name('candidate.update');
    Route::delete('delete-candidate/{id}','CandidateController@destroy')->name('candidate.destroy')->middleware(['can:delete-candidate']);
    //// Interview
    Route::get('list-interview','InterviewController@index')->name('interview.index')->middleware(['can:view-interview']);
    Route::get('create-interview','InterviewController@create')->name('interview.create')->middleware(['can:create-interview']);
    Route::post('store-interview','InterviewController@store')->name('interview.store');
    Route::get('edit-interview/{id}','InterviewController@edit')->name('interview.edit')->middleware(['can:edit-interview']);
    Route::post('update-interview/{id}','InterviewController@update')->name('interview.update');
    Route::delete('delete-interview/{id}','InterviewController@destroy')->name('interview.destroy')->middleware(['can:delete-interview']);
    // Job
    Route::get('list-job','JobController@index')->name('job.index')->middleware(['can:view-job']);
    //Job_candidate
    Route::get('job/{id}/job_candidate','JobController@job_create')->name('job.job_candidate.create');
    Route::post('store-job/job_candidate','JobController@job_store')->name('job.job_candidate.store');
    Route::get('list-job/{id}/job_candidate','JobController@job_index')->name('job.job_candidate.index');
    Route::get('list-candidate/{id}/job_candidate','JobController@job_list')->name('job.job_candidate.list');
    Route::get('job/{id}/add-candidate', 'JobController@addJobCandidate')->name('add.job.candidate');

    //end job_candidate
    Route::get('create-job','JobController@create')->name('job.create')->middleware(['can:create-job']);
    Route::post('store-job','JobController@store')->name('job.store');
    Route::get('edit-job/{id}','JobController@edit')->name('job.edit')->middleware(['can:edit-job']);
    Route::post('update-job/{id}','JobController@update')->name('job.update');
    Route::delete('delete-job/{id}','JobController@destroy')->name('job.destroy')->middleware(['can:delete-job']);
    // Role
    Route::get('list-role','RoleController@index')->name('role.index')->middleware(['can:view-role']);
    Route::get('create-role','RoleController@create')->name('role.create')->middleware(['can:create-role']);
    Route::post('store-role','RoleController@store')->name('role.store');
    Route::get('edit-role/{id}','RoleController@edit')->name('role.edit')->middleware(['can:edit-role']);
    Route::post('update-role/{id}','RoleController@update')->name('role.update');
    Route::delete('delete-role/{id}','RoleController@destroy')->name('role.destroy')->middleware(['can:delete-role']);

    //Work-Process
    Route::get('work-process/index','WorkProcessController@index')->name('process.candidate.index');
    Route::get('work-process/create','WorkProcessController@create')->name('process.candidate.create');
    Route::post('work-process/store','WorkProcessController@store')->name('process.candidate.store');

});
//Authenciation
//Route::get('/register-auth','AuthController@register')->name('register');
//Route::resource('role', 'RoleController')->middleware(['can:view-role','can:create-role','can:edit-role','can:delete-role']);
//Skill
Route::resource('skill','SkillController');
//Attribute
Route::resource('attribute','AttributeController');

Route::get('getAttribute','JobController@attribute')->name('getAttribute');

//API
Route::get('api-candidate/detail', 'Api\CandidateController@show')->name('api-candidate.detail');

Route::get('changeCandidate',"JobController@changeCandidate")->name('changeCandidate');
Route::post('rateCandidate','JobController@rateCandidate')->name('rateCandidate.create');


