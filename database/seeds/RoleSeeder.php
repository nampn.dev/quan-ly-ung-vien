<?php

use Illuminate\Database\Seeder;
use App\Role;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $author = Role::create([
           'name' => 'Author',
           'slug' => 'author',
            'permissions' =>json_encode([
                'create_recruitment'=>true,
            ]),
        ]);
    }
}
