<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoleProcessesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('role_processes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('work_process_id');
            $table->unsignedBigInteger('role_id')->nullable(true);
            $table->foreign('work_process_id')->references('id')->on('work_processes');
            $table->foreign('role_id')->references('id')->on('roles');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('role_processes');
    }
}
