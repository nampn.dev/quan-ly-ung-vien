@extends('admin.layout.main')
@section('content')
    @if (session()->has('success'))
        <div class="alert alert-success">
            @if(is_array(session('success')))
                @foreach (session('success') as $message)
                    {{ $message }}
                @endforeach
            @else
                {{ session('success') }}
            @endif
        </div>
    @endif
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark" style="font-family: 'Open Sans', sans-serif;">Job</h1>
                </div>
            </div><!-- /.row -->

        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <!-- MAIN CONTENT-->
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header" style=" border-bottom: none !important;">
                    <!-- Search form -->
                    <form action="{{route('job.index')}}" method="GET"
                          class="form-inline d-flex justify-content-center md-form form-sm active-purple active-purple-2 mt-2">
                        <div class="row" style="width: 100%">
                            <div class="col-6 d-flex pl-0">
                                @foreach($attributes as $attribute)
                                    <div class="rs-select2--light rs-select2--sm d-flex" style="margin-right: 10px;">
                                        <select class="js-select2" name="attribute[{{$attribute->name}}]">
                                            <option value="">{{$attribute->name}}</option>
                                            @foreach($attribute->option as $value)
                                                <option value="{{$value}}"
                                                        @if(in_array($value, $filter)) selected @endif>
                                                    {{$value}}
                                                </option>
                                            @endforeach
                                        </select>
                                        <div class="dropDownSelect2"></div>
                                    </div>
                                @endforeach
                                <div class="rs-select2--light rs-select2--sm d-flex" style="margin-right: 10px;">
                                    <select class="js-select2" name="status">
                                        <option value="">Status</option>
                                        <option
                                            @if(Request::get('status') == 0 && !is_null(Request::get('status'))) selected @endif>
                                            Hiện
                                        </option>
                                        <option @if(Request::get('status') == 1) selected @endif>Ẩn</option>
                                    </select>
                                    <div class="dropDownSelect2"></div>
                                </div>
                            </div>
                            <div class="col-6 d-flex justify-content-end pr-0">
                                <input class="form-control form-control-sm ml-3 w-75" type="text" placeholder="Search"
                                       aria-label="Search" @if(Request::get('key')) value="{{Request::get('key')}}"
                                       @endif name="key">
                                <button type="submit"><i class="fa fa-search"></i></button>
                            </div>
                        </div>
                        &nbsp
                        <a href="{{route('job.index')}}" class="au-btn--submit"><i class="fa fa-refresh"
                                                                                   aria-hidden="true"></i></a>
                    </form>
                    {{--                    //end search--}}


                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <table class="table table-bordered">
                        <thead>
                        <tr style="text-align: center;">
                            <th>ID</th>
                            <th>Title</th>
                            <th>Number</th>
                            <th>Skill</th>
                            <th>Address</th>
                            <th>Salary</th>
                            <th>Level</th>
                            <th>Attribute</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($jobs as $job)
                            <tr style="text-align: center;">
                                <td>{{$job->id}}</td>
                                <td>{{$job->title}}</td>
                                <td>{{$job->number}}</td>
                                <td>
                                    @foreach($job->skills as $skill)
                                        {{$skill->name}}
                                        @if(!$loop->last),@endif
                                    @endforeach
                                </td>
                                <td>{{$job->address}}</td>
                                <td>{{$job->salary}}</td>
                                <td>{{$job->level}}</td>
                                <td>
                                    @foreach($job->attribute as $attr)
                                        @foreach(json_decode($attr->pivot->value) as $att)
                                            {{$att}}
                                        @endforeach
                                    @endforeach
                                </td>
                                <td>
                                    @if($job->status==0) Hiện @else Ẩn @endif
                                </td>
                                <td>
                                    <div class="d-flex justify-content-center align-items-center">


                                        {{--@can('edit-user')--}}
                                        <a href="{{route('job.edit',['id'=>$job->id])}}" class="btn btn-info btn-sm">
                                            <i class="fas fa-pencil-alt"></i>
                                            Edit
                                        </a>
                                        {{--@endcan--}}
                                        <form action="{{route('job.destroy',['id'=>$job->id])}}"
                                              onclick="return confirm('Có muốn xóa không mà xóa?')" method="post"
                                              style="margin: 2px;">
                                            @method('DELETE')
                                            @csrf
                                            <button type="submit" class="btn btn-danger btn-sm">
                                                <i class="fas fa-trash"></i>
                                                Delete
                                            </button>
                                        </form>
                                        <a class="faq-links" href="{{route('job.job_candidate.index',['id'=>$job->id])}}">
                                            <span class="ui-icon ace-icon center bigger-110 blue btn btn-info btn-sm">
                                                 <i class="fas fa-plus"></i>
                                            </span>
                                        </a>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <div class="d-flex" style="padding: 10px;">
                        <div class="mx-auto">
                            {{$jobs->links()}}
                        </div>
                    </div>

                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
    </div>
    <!-- END DATA TABLE -->
@endsection
@section('script')
    <script>
        $('#exampleModal').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget)
            var recipient = button.data('whatever')
            var modal = $(this)
            modal.find('.modal-body input').val(recipient)
        })
    </script>
@endsection

