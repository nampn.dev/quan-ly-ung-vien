@extends('admin.layout.main')
@section('content')
    <style>
        .select2-search select2-search--inline select2-search__field{
            border: none;
        }
    </style>
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid" style="padding: 11px;">
            <div class="row mb-2">
                <div class="">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{route('admin.index')}}">Home</a></li>
                        <li class="breadcrumb-item"><a href="{{route('job.index')}}">List</a></li>
                        <li class="breadcrumb-item active"><a href="{{route('job.create')}}">Add</a></li>
                    </ol>
                </div>
                <div class=" col-sm-12">
                    <div class="table-data__tool-right" style="position: absolute;right: 0px;top: -25px;">
                        <a href="{{route('job.index')}}">
                            <button type="button" class="btn btn-info"><i class="fas fa-th-list mr-2"></i>List
                            </button>
                        </a>
                    </div>
                </div>
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->


        {{--Form Create--}}
        <div class="row" style="padding: 2px;">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <strong style="font-family: 'Open Sans', sans-serif;">Create Job</strong>
                    </div>
                    <div class="card-body card-block">
                        <form action="{{route('job.store')}}" method="post" enctype="multipart/form-data"
                              class="form-horizontal">
                            @csrf
                            <div class="form-group">
                                <label for="text-input" class=" form-control-label">Title</label>
                                <input value="{{old('title')}}" type="text"
                                       class="form-control @error('title') is-invalid @enderror" name="title"
                                       placeholder="Nhập tên đầy đủ">
                                @error('title')
                                <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                @enderror
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group ">
                                        <label for="" class=" form-control-label">Level</label>
                                        <input type="text"
                                               class="form-control @error('level') is-invalid @enderror"
                                               name="level"
                                               placeholder="Nhập vị trí cấp bậc cần tuyển">
                                        @error('level')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label for="name" class=" form-control-label">Status</label>
                                        <select class="form-control required @error('status') is-invalid @enderror" name="status"
                                                type="text">
                                            <option value="">Chọn trạng thái</option>
                                            <option value="0">Hiện</option>
                                            <option value="1">Ẩn</option>

                                        </select>
                                        @error('status')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                    <div class="form-group ">
                                        <label for="skill_id" class="form-control-label">Skill</label>
                                        <select  class="js-example-basic-multiple form-control @error('skill_id') is-invalid @enderror" name="skill_id[]"
                                                multiple="multiple form-control">
                                            @foreach($skills as $skill)
                                            <option value="{{$skill->id}}">{{$skill->name}}</option>
                                            @endforeach
                                        </select>
                                        @error('skill_id')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group ">
                                        <label class=" form-control-label">Number</label>
                                        <input value="{{old('number')}}" type="text"
                                               class="form-control @error('number') is-invalid @enderror"
                                               id="exampleInputEmail1" name="number" placeholder="Nhập số điện thoại nhà tuyển dụng">
                                        @error('number')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                    <div class="form-group ">
                                        <label for="confirm_password" class=" form-control-label">Address</label>
                                        <input type="text"
                                               class="form-control @error('address') is-invalid @enderror"
                                               name="address"
                                               placeholder="Nhập địa chỉ Cty">
                                        @error('address')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                    <div class="form-group ">
                                        <label for="confirm_password" class=" form-control-label">Salary</label>
                                        <input type="text"
                                               class="form-control @error('salary') is-invalid @enderror"
                                               name="salary"
                                               placeholder="Nhập mức lương">
                                        @error('salary')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                    <label class="form-control-label">Attribute</label>
                                    <select class="form-control m-b" id="category" name="attribute_id">
                                    @foreach($attributes as $attribute)
                                        <option value="{{$attribute->id}}">{{$attribute->name}}</option>
                                    @endforeach
                                    </select>
                                </div>
                                </div>
                            </div>
                            <div class="form-group ">
                                <label class=" form-control-label">Decription</label>
                                <textarea name="decription"
                                          class="form-control @error('decription') is-invalid @enderror"
                                          id="" cols="30" placeholder="Nhập nội dung"
                                          rows="10"></textarea>
                                @error('decription')
                                <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                @enderror
                            </div>

                            <div id="modalabc" class="modal" tabindex="-1">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title">Attribute </h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div id="content-modalabc" class="modal-body">

                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div style="margin-left: 921px;background-color: white;">
                                <button type="submit" class="btn btn-primary">Tạo</button>
                                <input type="reset" class="btn btn-default pull-right" value="Reset">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        CKEDITOR.replace('decription');

    </script>
    <script>
        $(document).ready(function() {
            $('.js-example-basic-multiple').select2();
        });
    </script>
    <script>
        $('#category').change(function (){
            var attribute_id = $(this).val()
            $.ajax({
                url:"{{route('getAttribute')}}",
                type:'GET',
                data:{attribute_id:attribute_id},
                success: function(data){
                    attr = ''
                     $.each(data, function(  index, attribute ) {
                        attr += '<input type="checkbox" name="attribute['+attribute_id+'][]" value="'+index+'"> <label>'+attribute+'</label>';
                        attr += '<div>'
                        // attribute.option.forEach(option =>{
                        //     attr += '<input type="checkbox" name="attribute['+attribute.id+'][]" value="'+option+'"> <span>'+option+'</span> <br>';
                        // });
                        attr += '</div>';
                        if(index != data.length-1){
                            attr += '<hr>';
                        }
                    });
                    let modal = $('#modalabc');
                    $('#content-modalabc').html(attr);
                    modal.modal('show');
                    console.log(data)
                }
            })
        })
    </script>
@endsection



