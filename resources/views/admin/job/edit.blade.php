@extends('admin.layout.main')
@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid" style="padding: 11px;">
            <div class="row mb-2">
                <div class="">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{route('admin.index')}}">Home</a></li>
                        <li class="breadcrumb-item"><a href="{{route('job.index')}}">List</a></li>
                        <li class="breadcrumb-item active"><a href="">Edit</a></li>
                    </ol>
                </div>
                <div class=" col-sm-12">
                    <div class="table-data__tool-right" style="position: absolute;right: 0px;top: -25px;">
                        <a href="{{route('job.index')}}">
                            <button type="button" class="btn btn-info"><i class="fas fa-th-list mr-2"></i>List
                            </button>
                        </a>
                    </div>
                </div>
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->


        {{--Form Create--}}
        <div class="row" style="padding: 2px;">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <strong style="font-family: 'Open Sans', sans-serif;">Edit User</strong>
                    </div>
                    <div class="card-body card-block">
                        <form action="{{route('job.update',['id'=>$jobs->id])}}" method="post"
                              enctype="multipart/form-data"
                              class="form-horizontal">
                            @csrf
                            <div class="form-group">
                                <label for="text-input" class=" form-control-label">Title</label>
                                <input value="{{$jobs->title}}" type="text"
                                       class="form-control @error('title') is-invalid @enderror" name="title"
                                       placeholder="Nhập tên đầy đủ">
                                @error('title')
                                <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                @enderror
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group ">
                                        <label for="" class=" form-control-label">Level</label>
                                        <input type="text" value="{{$jobs->level}}"
                                               class="form-control @error('level') is-invalid @enderror"
                                               name="level"
                                               placeholder="Nhập cấp bậc">
                                        @error('level')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label for="name" class=" form-control-label">Status</label>
                                        <select class="form-control @error('status') is-invalid @enderror" name="status"
                                                type="text">
                                            <option value="">Chọn trạng thái</option>
                                            <option>Hiện</option>
                                            <option>Ẩn</option>

                                        </select>
                                        @error('status')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                    <div class="form-group ">
                                        <label for="skill_id" class=" form-control-label">Skill</label>
                                        <select  class="js-example-basic-multiple form-control @error('skill_id') is-invalid @enderror" name="skill_id[]"
                                                 multiple="multiple form-control">
                                            @foreach($skills as $skill)
                                                <option value="{{$skill->id}}">{{$skill->name}}</option>
                                            @endforeach
                                        </select>
                                        @error('skill_id')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>

                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group ">
                                        <label class=" form-control-label">Number</label>
                                        <input value="{{$jobs->number}}" type="text"
                                               class="form-control @error('number') is-invalid @enderror"
                                               id="exampleInputEmail1" name="number" placeholder="Nhập số">
                                        @error('number')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                    <div class="form-group ">
                                        <label for="confirm_password" class=" form-control-label">Address</label>
                                        <input type="text" value="{{$jobs->address}}"
                                               class="form-control @error('address') is-invalid @enderror"
                                               name="address"
                                               placeholder="Nhập Lại Mật Khẩu">
                                        @error('address')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                    <div class="form-group ">
                                        <label for="confirm_password" class=" form-control-label">Salary</label>
                                        <input type="text" value="{{$jobs->salary}}"
                                               class="form-control @error('salary') is-invalid @enderror"
                                               name="salary"
                                               placeholder="Nhập Lại Mật Khẩu">
                                        @error('salary')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>


                                </div>
                            </div>
                            <div class="form-group ">
                                <label class=" form-control-label">Decription</label>
                                <textarea name="decription"
                                          class="form-control @error('decription') is-invalid @enderror"
                                          id="" cols="30" placeholder="Nhập nội dung"
                                          rows="10"></textarea>
                                @error('decription')
                                <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                @enderror
                            </div>

                            <div style="margin-left: 921px;background-color: white; ">
                                <button type="submit" class="btn btn-primary">Sửa</button>
                                <input type="reset" class="btn btn-default pull-right" value="Reset">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

