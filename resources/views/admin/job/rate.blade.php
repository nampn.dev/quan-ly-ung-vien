@extends('admin.layout.main')
@section('content')
    @if (session()->has('success'))
        <div class="alert alert-success">
            @if(is_array(session('success')))
                @foreach (session('success') as $message)
                    {{ $message }}
                @endforeach
            @else
                {{ session('success') }}
            @endif
        </div>
    @endif
    <style type="text/css">
        th,td{
            vertical-align: middle !important;
        }

    </style>
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark" style="font-family: 'Open Sans', sans-serif;">Danh sách ứng viên</h1>
                </div>
            </div><!-- /.row -->

        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <!-- MAIN CONTENT-->
    <div class="row">

        <div class="col-md-12">
            <div class="card">
                <div class="col-sm-6" style=" border-bottom: none !important;  width: 30%;">
                    <!-- Search form -->
                    <form style="margin-left: -49px;" class="form-inline d-flex justify-content-center md-form form-sm active-purple active-purple-2 mt-2">
                        <input class="form-control form-control-sm ml-3 w-75" type="text" placeholder="Search"
                               aria-label="Search" @if(Request::get('key')) value="{{Request::get('key')}}" @endif name="key">
                    </form>
                </div>
                <div class="col-sm-12">
                    {{--                    modal--}}
                    <button  type="button" class="btn btn-primary btn-sm" data-toggle="modal"
                             data-target="#exampleModal" data-whatever="@mdo"><i class="fas fa-plus"></i>
                    </button></div>
                <!-- /.card-header -->
                <div class="card-body">
                    <table class="table table-bordered" >
                        <thead>
                        <tr style="text-align: center;">
                            <th>ID</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Phone</th>
                            <th>Status</th>
                            <th>Đánh giá của người chỉ định</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($candidates as $candidate)
                            <tr style="text-align: center;">
                                <td>{{$candidate->id}}</td>
                                <td>{{$candidate->name}}</td>
                                <td>{{$candidate->email}}</td>
                                <td>{{$candidate->phone}}</td>
                                <td></td>
                                <td >


                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <div class="d-flex" style="padding: 10px;">
                        <div class="mx-auto">
{{--                            {{$candidates->links()}}--}}
                        </div>
                    </div>

                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>

    </div>
    <!-- END DATA TABLE -->
    <div class="modal fade" style="text-align: center;" id="exampleModal" tabindex="-1" role="dialog"
         aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <a href="{{route('candidate.create')}}" class="btn btn-secondary">
                        Thêm ứng viên
                    </a>
                    <button type="button" class="btn btn-primary">Thêm từ kho ứng
                        viên
                    </button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        function showDetail(id){
            $.ajax({
                url: "{{route('api-candidate.detail')}}",
                type: 'GET',
                data:{
                    id:id
                }
            }).done(function(response) {
                html = '';
                html += ''
            });
        }
    </script>
    <script type="text/javascript">
        //TOGGLE FONT AWESOME ON CLICK
        $('.faq-links').click(function() {
            $(this).find('i').toggleClass('fas fa-plus fas fa-minus')
        });
        $('.faq-links').blur(function() {
            $(this).find('i').toggleClass('fas fa-plus fas fa-minus')
        });

    </script>
@endsection

