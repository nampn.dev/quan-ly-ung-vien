@extends('admin.layout.main')
@section('content')
    @if (session()->has('success'))
        <div class="alert alert-success">
            @if(is_array(session('success')))
                @foreach (session('success') as $message)
                    {{ $message }}
                @endforeach
            @else
                {{ session('success') }}
            @endif
        </div>
    @endif
    <style type="text/css">
        th,td{
            vertical-align: middle !important;
        }

    </style>
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark" style="font-family: 'Open Sans', sans-serif;">List Candidate</h1>
                </div>
            </div><!-- /.row -->

        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- MAIN CONTENT-->
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header style">
                    <div class="ibox-content m-b-sm">
                        <form action="" method="GET">
                            <div class="row">
                                <div class="col-sm-8">
                                    <input type="text" id="search" name="search" value="" placeholder="Tìm kiếm..." class="form-control">
                                </div>

                                <div class="col-sm-1">
                                    <button type="submit" class="btn btn-success"><i class="fa fa-search"></i>
                                    </button>
                                </div>
                                <div class="col-sm-3 ">
                                    <button type="button" id="open-model" style="text-align: center;width: 100%"
                                            class="btn btn-info" data-toggle="modal"
                                            data-target="#modal-employee-selected"><i
                                            class="fa item-cc"></i>Ứng viên đã thêm
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <table class="table table-bordered" >
                        <thead>
                        <tr style="text-align: center;">
                            <th>ID</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Phone</th>
                            <th>Date </th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($candidates as $candidate)
                            <tr style="text-align: center;">
                                <td>{{$candidate->id}}</td>
                                <td>{{$candidate->name}}</td>
                                <td>{{$candidate->email}}</td>
                                <td>{{$candidate->phone}}</td>
                                <td>{{$candidate->date_of_birth}}</td>
                                <td id="td-add-candidate-{{$candidate->id}}">
                                        <a class="faq-links" id="button-add-candidate-{{$candidate->id}}" data-id = {{$candidate->id}}>
                                            <span class="ui-icon ace-icon center bigger-110 blue btn btn-info btn-sm">
                                                 <i class="fas fa-plus" ></i>
                                            </span>
                                        </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <div class="d-flex" style="padding: 10px;">
                        <div class="mx-auto">
                            {{$candidates->links()}}
                        </div>
                    </div>

                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
    </div>
    <!-- END DATA TABLE -->
    {{--Modal--}}
    <div class="modal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Modal title</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>Modal body text goes here.</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary">Save changes</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
{{--Modal ứng viên đã thêm--}}
    <div class="container">
        <div class="modal fade bd-example-modal-lg" id="modal-employee-selected" role="dialog" data-backdrop="static">
            <div class="modal-dialog modal-lg">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <p class="modal-title" style="font-size: 24px">{{__('Danh sách ứng viên đã chọn')}}</p>
                    </div>
                    <div class="modal-body table-responsive" id="employee-selected" style="padding-left: 15px ; padding-top: 0px">
                        <form class="form-horizontal" action="{{route('add.job.candidate', ['id'=>$id])}}">
                            @csrf
                            <table class="footable table table-stripped table-bordered table-responsive toggle-arrow-tiny table-job"
                                   data-page-size="15">
                                <thead>
                                <tr>
                                    <th>{{__('Họ tên ứng viên')}}</th>
                                    <th>{{__('Email')}}</th>
                                    <th>{{__('Số điện thoại')}}</th>
                                    <th  style="width: 11%;text-align: center">{{__('Hành động')}}</th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                            <div class="form-group form-inline pull-right">

                                <button type="button" class="btn btn-danger" id="close-modal" data-dismiss="modal" title="{{__('Đóng')}}">{{__('Đóng')}}</button>
                                <button type="submit" class="btn btn-primary add-employee" title="{{__('Lưu')}}">{{__('Lưu')}}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{--    End ứng viên đã thêm--}}
    <style>
        .color{
            color: black;
        }
    </style>
@endsection

@section('script')
    <script>
        let arr_candidate = [];

        function showDetail(id){
            $.ajax({
                url: "{{route('api-candidate.detail')}}",
                type: 'GET',
                data:{
                    id:id
                }
            }).done(function(response) {
                html = '';
                html += ''
            });
        }
    </script>
    <script type="text/javascript">
        //Add candidate
        $('.faq-links').click(function() {
            let candidateId=$(this).data("id");
            let check = check_exist(candidateId);
            if(check == "not_exist"){
                $.ajax({
                    type : "GET",
                    data : {
                        'id' : candidateId
                    },
                    url : "{{route('changeCandidate')}}",
                    success : function (response){
                        var html = ''
                        html+='<tr id="candidate-added-'+ response.id +'">' +
                            '<td>'+response.name+'</td>' +
                            '<td>'+response.email+'</td>' +
                            '<td>'+response.phone+'</td>' +
                            '<td>'+'<button type="button" class="btn btn-danger remove-candidate" onclick="return sub_cadidate(this)" data-id="1" title="Xoá ứng viên"><i class="fa item-cc fa-minus"></i></button>'+'</td>'+
                            '<input value="'+response.id+'" type="hidden" name = "id[]">'
                        '</tr>'
                        $('#modal-employee-selected tbody').append(html)
                        arr_candidate.push(response.id);
                        let button_minus = `
                                            <a class="btn-sub-candidate" id="btn-sub-candidate-`+response.id+`" onclick="return sub_cadidate(this)" data-id="`+ candidateId +`">
                                                <button class="btn btn-danger">
                                                       <i class="fa fa-minus"></i>
                                                </button>
                                            </a>
                                        `;
                        $('#button-add-candidate-'+ candidateId ).remove();
                        $('#td-add-candidate-'+candidateId).append(button_minus);
                    }
                });
                return false;
            }
        });
        function check_exist(id){
            if(arr_candidate.includes(id)){
                return "exist";
            }else{
                return "not_exist";
            }
        }

        function sub_cadidate(e){
            let id_to_sub = $(e).data('id');
            $('#candidate-added-'+id_to_sub).remove();
            let re_btn_add_candidate = `
                <a class="faq-links" onclick="return add_candidate_reload(this)" id="button-add-candidate-`+id_to_sub+`" data-id="`+id_to_sub+`" >
                   <span class="ui-icon ace-icon center bigger-110 blue btn btn-info btn-sm">
                       <i class="fas fa-plus" ></i>
                   </span>
                </a>
            `;
            $('#btn-sub-candidate-'+id_to_sub).remove();
            $('#td-add-candidate-'+id_to_sub).append(re_btn_add_candidate);
        }
    </script>
    <style>
        .style {
            border-bottom: none;
        }
    </style>
@endsection

