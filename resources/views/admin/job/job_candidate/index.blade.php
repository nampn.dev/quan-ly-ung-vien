@extends('admin.layout.main')
@section('content')
    @if (session()->has('success'))
        <div class="alert alert-success">
            @if(is_array(session('success')))
                @foreach (session('success') as $message)
                    {{ $message }}
                @endforeach
            @else
                {{ session('success') }}
            @endif
        </div>
    @endif
    <style type="text/css">
        th, td {
            vertical-align: middle !important;
        }

    </style>
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark" style="font-family: 'Open Sans', sans-serif;">Danh sách ứng viên</h1>
                </div>
            </div><!-- /.row -->

        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    {{--    Hiển thị Job--}}
    <div class="wrapper wrapper-content animated fadeInRight ecommerce card card-body">
    <div class="row">
                <div class="row">
                    <div class="col-sm-12">
                        <h1>{{$job->title}}</h1><br>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="title">Level:</label>
                            <span>{{$job->level}}</span>
                        </div>
                        <div class="form-group">
                            <label class="title">Address:</label>
                            <span>{{$job->address}}</span>
                        </div>

                    </div>

                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="title">Skill:</label>
                            @foreach($job->skills as $skill)
                                <span class="label label-secondary" >
                            {{$skill->name}}
                        </span>&nbsp
                            @endforeach
                        </div>
                        <div class="form-group">
                            <label class="title">Phone:</label>
                            <span>{{$job->number}}</span>
                        </div>
                        <div class="form-group">
                            <label class="title">Salary:</label>
                            <span>{{$job->salary}}</span>
                        </div>
                    </div>
                </div>
    </div>
    </div>
    {{--    End hiển thị--}}

    <!-- MAIN CONTENT-->
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header style">
                    <div class="ibox-content m-b-sm">
                        <form action="" method="GET">
                            <div class="row">
                                <div class="col-sm-8">
                                    <input type="text" id="search" name="search" value="" placeholder="Tìm kiếm..."
                                           class="form-control">
                                </div>

                                <div class="col-sm-1">
                                    <button type="submit" class="btn btn-success"><i class="fa fa-search"></i>
                                    </button>
                                </div>
                                <div class="col-sm-3 ">
                                    <button type="button" class="btn btn-info pull-right" data-toggle="modal"
                                            data-target="#exampleModal" data-whatever="@mdo"><i class="fas fa-plus"></i>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <table class="table table-bordered">
                        <thead>
                        <tr style="text-align: center;">
                            <th>ID</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Phone</th>
                            <th>Trạng thái</th>
                            <th>Đánh giá của người chỉ định</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($job->candidates as $candidate)
                            <tr style="text-align: center;">
                                <td>{{$candidate->id}}</td>
                                <td>{{$candidate->name}}</td>
                                <td>{{$candidate->email}}</td>
                                <td>{{$candidate->phone}}</td>
                                <td>@foreach($a as $item)
                                       @if($item->candidate_id == $candidate->id)
                                          @if($item->status == 1)
                                              <span class="btn btn-primary">Pending</span>
                                            @endif
                                        @endif
                                    @endforeach
                                </td>
                                <td>
                                   @if(($candidate->jobCandidate->where('job_id', $job->id)->first()->status) == 1)
                                        <button class="btn btn-secondary btn-evaluate" data-toggle="modal"
                                                data-target="#viewModal" onclick="$('#candidate_id').val({{$candidate->id}})">{{__('Đánh giá')}}
                                        </button>
                                       @elseif(($candidate->jobCandidate->where('job_id', $job->id)->first()->status) == 3)
                                            Fail
                                       @else
                                            Pass
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <div class="d-flex" style="padding: 10px;">
                        <div class="mx-auto">
                            {{--                            {{$candidates->links()}}--}}
                        </div>
                    </div>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>

    </div>
    <!-- END DATA TABLE -->
    {{--    Modal thêm ứng viên--}}
    <div class="modal fade" style="text-align: center;" id="exampleModal" tabindex="-1" role="dialog"
         aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <a href="{{route('job.job_candidate.create',['id'=>$job->id])}}" class="btn btn-secondary">
                        Thêm ứng viên
                    </a>
                    <a href="{{route('job.job_candidate.list',['id'=>$job->id])}}" class="btn btn-primary">
                        Thêm từ kho ứng viên
                    </a>
                </div>
            </div>
        </div>
    </div>
    {{--    End Modal thêm ứng viên--}}
{{--    Modal đánh giá--}}
    <div class="modal fade" style="text-align: center;" id="viewModal" tabindex="-1" role="dialog"
         aria-labelledby="exampleModalLabel" aria-hidden="true">
        <!-- Modal content-->
        <div class="modal-content modal-dialog modal-lg container">
            <div class="modal-header">
                <h1 class="modal-title">Đánh giá</h1>
            </div>
            <div class="modal-body" id="employee-selected" style="padding-left: 15px ; padding-top: 0px">
                    <table class="table table-bordered" id="tab"
                           data-page-size="15">
                        <tr>
                            <td>{{__('Họ tên ứng viên')}}</td>
                            <td>
                                {{$job->name}}
                            </td>
                        </tr>
                        <tr>
                            <td>{{__('Tiêu đề công viêc')}}</td>
                            <td>{{$job->title}}</td>
                        </tr>
                        <tr>
                            <td>{{__('Cấp bậc')}}</td>
                            <td>{{$job->level}}</td>
                        </tr>
                    </table>
                    <input type="hidden" name="user_id" value="{{auth()->user()->id}}">
                    <input type="hidden" name="job_id" value="">

                <label class="control-label" style="margin-right: 615px;">{{__('Kết quả đánh giá')}} <span class="text-danger">*</span></label>
                <form action="{{route('rateCandidate.create')}}" method="post">
                    @csrf
                    <div class="wrapper wrapper-content animated ecommerce card">
                        <div class="ibox-content m-b-sm">

                                <div class="form-group" style="padding: 40px;">
                                    <label for="job_category">Chi tiết quy trình : </label>
                                    <div class="row">
                                        <div class="col-md-1 col-sm-1 stt-right">STT</div>
                                        <div class="col-md-6 col-sm-5">
                                            <p>Tên bước <span class="column-required">*</span></p>
                                        </div>
                                        <div class="col-md-3 col-sm-4">
                                            <p>Đánh giá <span class="column-required">*</span></p>
                                        </div>
                                    </div>
                                    <div id="form-step">
                                        @php $i = 0; @endphp
                                        @foreach($workProcess as $step)
                                            <input type="hidden" name="candidate_id" id="candidate_id" value="">
                                            <input type="hidden" name="job_id" value="{{$job->id}}">
                                            @php $i++; @endphp
                                            @if(auth()->user()->role_id==1 && $step->name=='Check CV')
                                            <div class="row step">
                                                <div class="col-md-1 col-sm-1 index-step stt-right">{{$loop->index+1}}</div>
                                                <div class="col-md-6 col-sm-5">
                                                    @if($step->name=='Check CV')
                                                    <input type="text"
                                                           class="form-control  step_name" maxlength="191"
                                                           value="{{$step->name}}">
                                                        <input type="hidden" name="process[{{$i}}][work_process_id]" value="{{$step->id}}">
                                                    @endif
                                                </div>
                                                <div class="col-md-3 col-sm-4">
                                                    <select
                                                        class="form-control" name="process[{{$i}}][status]">
                                                        <option value="">Chọn đánh giá</option>
                                                        <option value="3">Pass</option>
                                                        <option value="2">Fail</option>
                                                    </select>

                                                </div>

                                            </div>
                                            @endif
                                            @if(auth()->user()->role_id==2 && $step->name=='Phone Screen')
                                            <div class="row step">
                                                <div class="col-md-1 col-sm-1 index-step stt-right">{{$loop->index+1}}</div>
                                                <div class="col-md-6 col-sm-5">
                                                    @if($step->name=='Phone Screen')
                                                    <input type="text"
                                                           class="form-control  step_name" maxlength="191"
                                                           value="{{$step->name}}">
                                                        <input type="hidden" name="process[{{$i}}][work_process_id]" value="{{$step->id}}">
                                                    @endif
                                                </div>
                                                <div class="col-md-3 col-sm-4">
                                                    <select
                                                        class="form-control" name="process[{{$i}}][status]">
                                                        <option value="">Chọn đánh giá</option>
                                                        <option value="3">Pass</option>
                                                        <option value="2">Fail</option>
                                                    </select>

                                                </div>

                                            </div>
                                            @endif
                                            @if(auth()->user()->role_id==3 && $step->name=='Interview')
                                            <div class="row step">
                                                <div class="col-md-1 col-sm-1 index-step stt-right">{{$loop->index+1}}</div>
                                                <div class="col-md-6 col-sm-5">
                                                    @if($step->name=='Interview')
                                                    <input type="text"
                                                           class="form-control  step_name" maxlength="191"
                                                           value="{{$step->name}}">
                                                        <input type="hidden" name="process[{{$i}}][work_process_id]" value="{{$step->id}}">
                                                    @endif
                                                </div>
                                                <div class="col-md-3 col-sm-4">
                                                    <select
                                                        class="form-control" name="process[{{$i}}][status]">
                                                        <option value="">Chọn đánh giá</option>
                                                        <option value="3">Pass</option>
                                                        <option value="2">Fail</option>
                                                    </select>

                                                </div>

                                            </div>
                                            @endif
                                            @if(auth()->user()->role_id==2 && $step->name=='Offer')
                                            <div class="row step">
                                                <div class="col-md-1 col-sm-1 index-step stt-right">{{$loop->index+1}}</div>
                                                <div class="col-md-6 col-sm-5">
                                                    @if($step->name=='Offer')
                                                    <input type="text"
                                                           class="form-control  step_name" maxlength="191"
                                                           value="{{$step->name}}">
                                                        <input type="hidden" name="process[{{$i}}][work_process_id]" value="{{$step->id}}">
                                                    @endif
                                                </div>
                                                <div class="col-md-3 col-sm-4">
                                                    <select
                                                        class="form-control" name="process[{{$i}}][status]">
                                                        <option value="">Chọn đánh giá</option>
                                                        <option value="3">Pass</option>
                                                        <option value="2">Fail</option>
                                                    </select>

                                                </div>

                                            </div>
                                            @endif
                                            @if(auth()->user()->role_id==1 && $step->name=='On_broadingg')
                                            <div class="row step">
                                                <div class="col-md-1 col-sm-1 index-step stt-right">{{$loop->index+1}}</div>
                                                <div class="col-md-6 col-sm-5">
                                                    @if($step->name=='On_broadingg')
                                                    <input type="text"
                                                           class="form-control  step_name" maxlength="191"
                                                           value="{{$step->name}}">
                                                        <input type="hidden" name="process[{{$i}}][work_process_id]" value="{{$step->id}}">
                                                    @endif
                                                </div>
                                                <div class="col-md-3 col-sm-4">
                                                    <select
                                                        class="form-control" name="process[{{$i}}][status]">
                                                        <option value="">Chọn đánh giá</option>
                                                        <option value="3">Pass</option>
                                                        <option value="2">Fail</option>
                                                    </select>

                                                </div>

                                            </div>
                                            @endif
                                        @endforeach
                                    </div>
                                </div>

                        </div>
                    </div>
                    <div class="form-group row mb-0 btn-review" style="margin-left: 627px;">
                        <div class="pull-right">
                            <button type="submit" class="btn btn-primary" onclick="validateReview()"
                                    title="{{__('Lưu')}}">{{__('Lưu')}}</button>
                        </div>
                        <div class="pull-right">
                            <button type="button" class="btn btn-danger" id="close-modal" data-dismiss="modal"
                                    title="{{__('Đóng')}}">{{__('Đóng')}}</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
{{--    End modal đánh giá--}}

@endsection

@section('script')
    <script>
        function showDetail(id) {
            $.ajax({
                url: "{{route('api-candidate.detail')}}",
                type: 'GET',
                data: {
                    id: id
                }
            }).done(function (response) {
                html = '';
                html += ''
            });
        }
    </script>
    <script type="text/javascript">
        //TOGGLE FONT AWESOME ON CLICK
        $('.faq-links').click(function () {
            $(this).find('i').toggleClass('fas fa-plus fas fa-minus')
        });

    </script>
    <style>
        .style {
            border-bottom: none;
        }
    </style>
@endsection
{{--                    modal--}}


