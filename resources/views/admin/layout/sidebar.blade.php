<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="index3.html" class="brand-link">
        <img src="{{asset('dist/img/AdminLTELogo.png')}}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
             style="opacity: .8">
        <span class="brand-text font-weight-light">AdminLTE 3</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
                <img src="{{asset('dist/img/user2-160x160.jpg')}}" class="img-circle elevation-2" alt="User Image">
            </div>
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <li class="nav-item has-treeview">
                    <a href="#" class="nav-link">
                        <p>
                            {{ Auth::user()->name }}
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a style="color: white;" class="dropdown-item" href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                {{ __('Logout') }}
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                {{--admin--}}
                @can('view-user')
                    <li class="nav-item has-treeview">
                        <a href="#" class="nav-link">
                            <i class="nav-icon fas fa-user"></i>
                            <p>
                                Admin
                                <i class="right fas fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            @can('view-user')
                                <li class="nav-item">
                                    <a href="{{route('user.index')}}" class="nav-link">
                                        <i class="nav-icon fas fa-address-card"> </i>
                                        <p>Danh sách</p>
                                    </a>
                                </li>
                            @endcan
                            @can('create-user')
                                <li class="nav-item">
                                    <a href="{{route('user.create')}}" class="nav-link">
                                        <i class="nav-icon fas fa-plus"></i>
                                        <p>Thêm Mới</p>
                                    </a>
                                </li>
                            @endcan
                        </ul>
                    </li>
                @endcan
                {{--                @endcan--}}
                {{--                recruitment--}}
                @can('view-recruitment')
                    <li class="nav-item has-treeview">
                        <a href="#" class="nav-link">
                            <i class="nav-icon fas fa-user-tie"></i>
                            <p>
                                Recruitment
                                <i class="fas fa-angle-left right"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            @can('view-recruitment')
                                <li class="nav-item">
                                    <a href="{{route('recruitment.index')}}" class="nav-link">
                                        <i class="nav-icon fas fa-address-card"> </i>
                                        <p>Danh sách </p>
                                    </a>
                                </li>
                            @endcan
                            @can('create-recruitment')
                                <li class="nav-item">
                                    <a href="{{route('recruitment.create')}}" class="nav-link">
                                        <i class="nav-icon fas fa-plus"></i>
                                        <p>Thêm Mới </p>
                                    </a>
                                </li>
                            @endcan
                        </ul>
                    </li>
                @endcan
                @can('view-candidate')
                <li class="nav-item has-treeview">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-user-tie"></i>
                        <p>
                            Candidate
                            <i class="fas fa-angle-left right"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
{{--                        @can('view-recruitment')--}}
                            <li class="nav-item">
                                <a href="{{route('candidate.index')}}" class="nav-link">
                                    <i class="nav-icon fas fa-address-card"> </i>
                                    <p>Danh sách </p>
                                </a>
                            </li>
{{--                        @endcan--}}
{{--                        @can('create-recruitment')--}}
                            <li class="nav-item">
                                <a href="{{route('candidate.create')}}" class="nav-link">
                                    <i class="nav-icon fas fa-plus"></i>
                                    <p>Thêm Mới </p>
                                </a>
                            </li>
{{--                        @endcan--}}
                    </ul>
                </li>
                @endcan
                @can('view-interview')
                    <li class="nav-item has-treeview">
                        <a href="#" class="nav-link">
                            <i class="nav-icon fas fa-glasses"></i>
                            <p>
                                Interview
                                <i class="fas fa-angle-left right"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            @can('view-interview')
                                <li class="nav-item">
                                    <a href="{{route('interview.index')}}" class="nav-link">
                                        <i class="nav-icon fas fa-address-card"> </i>
                                        <p>Danh sách </p>
                                    </a>
                                </li>
                            @endcan
                            @can('create-interview')
                                <li class="nav-item">
                                    <a href="{{route('interview.create')}}" class="nav-link">
                                        <i class="nav-icon fas fa-plus"></i>
                                        <p>Thêm Mới </p>
                                    </a>
                                </li>
                            @endcan
                        </ul>
                    </li>
                @endcan
                @can('view-role')
                    <li class="nav-item has-treeview">
                        <a href="#" class="nav-link">
                            <i class="nav-icon fas fa-atom"></i>
                            <p>
                                Role
                                <i class="fas fa-angle-left right"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            @can('view-role')
                                <li class="nav-item">
                                    <a href="{{route('role.index')}}" class="nav-link">
                                        <i class="nav-icon fas fa-address-card"> </i>
                                        <p>Danh sách </p>
                                    </a>
                                </li>
                            @endcan
                            @can('create-role')
                                <li class="nav-item">
                                    <a href="{{route('role.create')}}" class="nav-link">
                                        <i class="nav-icon fas fa-plus"></i>
                                        <p>Thêm Mới </p>
                                    </a>
                                </li>
                            @endcan
                        </ul>
                    </li>
                @endcan
                @can('view-job')
                    <li class="nav-item has-treeview">
                        <a href="#" class="nav-link">
                            <i class="nav-icon fas fa-atom"></i>
                            <p>
                                Job
                                <i class="fas fa-angle-left right"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            @can('view-job')
                                <li class="nav-item">
                                    <a href="{{route('job.index')}}" class="nav-link">
                                        <i class="nav-icon fas fa-address-card"> </i>
                                        <p>Danh sách </p>
                                    </a>
                                </li>
                            @endcan

                            @can('create-job')
                                <li class="nav-item">
                                    <a href="{{route('job.create')}}" class="nav-link">
                                        <i class="nav-icon fas fa-plus"></i>
                                        <p>Thêm Mới </p>
                                    </a>
                                </li>
                            @endcan
                        </ul>
                    </li>
                @endcan

                <li class="nav-item has-treeview">
                    <a href="{{ route('process.candidate.create') }}" class="nav-link">
                        <i class="nav-icon fas fa-atom"></i>
                        <p>
                           Process
                        </p>
                    </a>
                </li>

                @can('view-skill','create-skill')
                    <li class="nav-item has-treeview">
                        <a href="#" class="nav-link">
                            <i class="nav-icon fas fa-atom"></i>
                            <p>
                                Skill
                                <i class="fas fa-angle-left right"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            @can('view-skill')
                                <li class="nav-item">
                                    <a href="{{route('skill.index')}}" class="nav-link">
                                        <i class="nav-icon fas fa-address-card"> </i>
                                        <p>Danh sách </p>
                                    </a>
                                </li>
                            @endcan
                            @can('create-skill')
                                <li class="nav-item">
                                    <a href="{{route('skill.create')}}" class="nav-link">
                                        <i class="nav-icon fas fa-plus"></i>
                                        <p>Thêm Mới </p>
                                    </a>
                                </li>
                            @endcan
                        </ul>
                    </li>
                @endcan
                @can('view-attribute','create-attribute')
                    <li class="nav-item has-treeview">
                        <a href="#" class="nav-link">
                            <i class="nav-icon fas fa-atom"></i>
                            <p>
                                Attribute
                                <i class="fas fa-angle-left right"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            @can('view-attribute')
                                <li class="nav-item">
                                    <a href="{{route('attribute.index')}}" class="nav-link">
                                        <i class="nav-icon fas fa-address-card"> </i>
                                        <p>Danh sách </p>
                                    </a>
                                </li>
                            @endcan
                            @can('create-attribute')
                                <li class="nav-item">
                                    <a href="{{route('attribute.create')}}" class="nav-link">
                                        <i class="nav-icon fas fa-plus"></i>
                                        <p>Thêm Mới </p>
                                    </a>
                                </li>
                            @endcan
                        </ul>
                    </li>
                @endcan
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
