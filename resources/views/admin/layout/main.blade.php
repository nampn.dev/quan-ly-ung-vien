
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="x-ua-compatible" content="ie=edge">

	<title>AdminLTE 3 | Dashboard 2</title>
    <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}" />
    <link rel="stylesheet" href="{{asset('css/style.css')}}">
    <link rel="stylesheet" href="{{asset('css/work-process.css')}}">
    <link rel="icon" href="img/cssoft_logo_icon.png" type="image/gif" sizes="100x100">
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.css" />
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">

	<!-- Font Awesome Icons -->
	<link rel="stylesheet" href="{{asset('plugins/fontawesome-free/css/all.min.css')}}">
	<!-- overlayScrollbars -->
	<link rel="stylesheet" href="{{asset('plugins/overlayScrollbars/css/OverlayScrollbars.min.css')}}">
	<!-- Theme style -->
	<link rel="stylesheet" href="{{asset('dist/css/adminlte.min.css')}}">
	<!-- Google Font: Source Sans Pro -->
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
{{--    //dual--}}
    <!-- page specific plugin styles -->
    <link rel="stylesheet" href="{{asset('assets/css/bootstrap-duallistbox.min.css')}}" />

</head>
<body class="hold-transition sidebar-mini layout-fixed layout-navbar-fixed layout-footer-fixed" style="overflow-x: hidden">
	<div class="wrapper">
		@include('admin.layout.header')
		@include('admin.layout.sidebar')
		@include('admin.layout.topbar')
		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			@yield('content')
		</div>
		<!-- /.content-wrapper -->
		@include('admin.layout.footer')
	</div>
	<!-- ./wrapper -->

	<!-- REQUIRED SCRIPTS -->
	<!-- jQuery -->
	<script src="{{asset('plugins/jquery/jquery.min.js')}}"></script>
	<!-- Bootstrap -->
	<script src="{{asset('plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
	<!-- overlayScrollbars -->
	<script src="{{asset('plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js')}}"></script>
	<!-- AdminLTE App -->
	<script src="{{asset('dist/js/adminlte.js')}}"></script>
	<!-- OPTIONAL SCRIPTS -->
	<script src="{{asset('dist/js/demo.js')}}"></script>

	<!-- PAGE PLUGINS -->
	<!-- jQuery Mapael -->
	<script src="{{asset('plugins/jquery-mousewheel/jquery.mousewheel.js')}}"></script>
	<script src="{{asset('plugins/raphael/raphael.min.js')}}"></script>
	<script src="{{asset('plugins/jquery-mapael/jquery.mapael.min.js')}}"></script>
	<script src="{{asset('plugins/jquery-mapael/maps/usa_states.min.js')}}"></script>
	<!-- ChartJS -->
	<script src="{{asset('plugins/chart.js/Chart.min.js')}}"></script>
    <script src="//cdn.ckeditor.com/4.15.1/standard/ckeditor.js"></script>
    <script src="{{asset('dist/js/pages/dashboard2.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/jquery-3.4.1.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/bootstrap.bundle.min.js')}}"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.bundle.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
{{--    //dual--}}

    <!-- page specific plugin scripts -->
    <script src="{{asset('assets/js/jquery.bootstrap-duallistbox.min.js')}}"></script>
    <script src="{{asset('assets/js/bootstrap-multiselect.min.js')}}"></script>
    <script src="{{asset('assets/js/select2.min.js')}}"></script>

    @yield('script')
</body>
</html>
