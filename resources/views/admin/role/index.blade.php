@extends('admin.layout.main')
@section('content')
    @if (session()->has('success'))
        <div class="alert alert-success">
            @if(is_array(session('success')))
                @foreach (session('success') as $message)
                    {{ $message }}
                @endforeach
            @else
                {{ session('success') }}
            @endif
        </div>
    @endif
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark" style="font-family: 'Open Sans', sans-serif;">Role</h1>
                </div>
            </div><!-- /.row -->

        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <!-- MAIN CONTENT-->
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header" style=" border-bottom: none !important;  width: 30%;">
                    <!-- Search form -->
                    <form style="margin-left: -49px;"
                          class="form-inline d-flex justify-content-center md-form form-sm active-purple active-purple-2 mt-2">
                        <input class="form-control form-control-sm ml-3 w-75" type="text" placeholder="Search"
                               aria-label="Search" @if(Request::get('key')) value="{{Request::get('key')}}"
                               @endif name="key">
                    </form>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <table class="table table-bordered">
                        <thead>
                        <tr style="text-align: center;">
                            <th>ID</th>
                            <th>Name</th>
                            <th>Permission</th>
                            <th style="width: 20%;">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($roles as $role)
                            <tr style="text-align: center;">
                                <td>{{$role->id}}</td>
                                <td>{{$role->name}}</td>
                                <td>
                                    @foreach($role->permissions as $value)
                                        <span>{{$value->name}}</span>
                                    @endforeach
                                </td>
                                <td >
                                    {{--                                    @can('edit-user')--}}
                                    <div class="d-flex justify-content-center align-items-center">
                                    <a href="{{route('role.edit',['id'=>$role->id])}}" class="btn btn-info btn-sm">
                                        <i class="fas fa-pencil-alt"></i>
                                        Edit </a>
                                    {{--                                    @endcan--}}
                                    <form action="{{route('role.destroy',['id'=>$role->id])}}"
                                          onclick="return confirm('Có muốn xóa không mà xóa?')" method="post"
                                          style="margin: 2px;">
                                        @method('DELETE')
                                        @csrf
                                        <button type="submit" class="btn btn-danger btn-sm">
                                            <i class="fas fa-trash"></i>
                                            Delete
                                        </button>
                                    </form>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <div class="d-flex" style="padding: 10px;">
                        <div class="mx-auto">
                            {{$roles->links()}}
                        </div>
                    </div>

                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
    </div>
    <!-- END DATA TABLE -->
@endsection
