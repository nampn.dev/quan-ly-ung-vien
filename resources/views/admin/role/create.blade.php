@extends('admin.layout.main')
@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid" style="padding: 11px;">
            <div class="row mb-2">
                <div class="">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{route('admin.index')}}">Home</a></li>
                        <li class="breadcrumb-item"><a href="{{route('role.index')}}">List</a></li>
                        <li class="breadcrumb-item active"><a href="{{route('role.create')}}">Add</a></li>
                    </ol>
                </div>
                <div class=" col-sm-12">
                    <div class="table-data__tool-right" style="position: absolute;right: 0px;top: -25px;">
                        <a href="{{route('role.index')}}">
                            <button type="button" class="btn btn-info"><i class="fas fa-th-list mr-2"></i>List
                            </button>
                        </a>
                    </div>
                </div>
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->


        {{--Form Create--}}
        <div class="row" style="padding: 2px;">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <strong style="font-family: 'Open Sans', sans-serif;">Create Role</strong>
                    </div>
                    <div class="card-body card-block">
                        <form action="{{route('role.store')}}" method="post" enctype="multipart/form-data"
                              class="form-horizontal">
                            @csrf
                            <div class="form-group">
                                <label for="text-input" class=" form-control-label">Name</label>
                                <input value="{{old('name')}}" type="text"
                                       class="form-control @error('name') is-invalid @enderror" name="name"
                                       placeholder="Nhập tên đầy đủ">
                                @error('name')
                                <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                @enderror
                            </div>
                            {{--                            Role options--}}
                            <div class="form-group">
                                <label for="multiple-select" class=" form-control-label">Permission</label>
                                <select name="permission[]" id="multiple-select" multiple="" class="form-control" style="height: 250px;">
                                    @foreach($permissions as $permission)
                                        <option value="{{$permission->id}}">
                                            {{$permission->name}}
                                        </option>
                                    @endforeach()
                                </select>

                            </div>

                            <div style="margin-left: 921px;background-color: white;">
                                <button type="submit" class="btn btn-primary">Tạo</button>
                                <input type="reset" class="btn btn-default pull-right" value="Reset">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection



