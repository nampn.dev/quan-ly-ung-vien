@extends('admin.layout.main')
@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid" style="padding: 11px;">
            <div class="row mb-2">
                <div class="">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{route('admin.index')}}">Home</a></li>
                        <li class="breadcrumb-item"><a href="{{route('interview.index')}}">List</a></li>
                        <li class="breadcrumb-item active"><a href="">Edit</a></li>
                    </ol>
                </div>
                <div class=" col-sm-12">
                    <div class="table-data__tool-right" style="position: absolute;right: 0px;top: -25px;">
                        <a href="{{route('interview.index')}}">
                            <button type="button" class="btn btn-info"><i class="fas fa-th-list mr-2"></i>List
                            </button>
                        </a>
                    </div>
                </div>
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->


        {{--Form Create--}}
        <div class="row" style="padding: 2px;">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <strong style="font-family: 'Open Sans', sans-serif;">Edit Interview</strong>
                    </div>
                    <div class="card-body card-block">
                        <form action="{{route('interview.update',['id'=>$interview->id])}}" method="post"
                              enctype="multipart/form-data"
                              class="form-horizontal">
                            @csrf
                            <div class="form-group">
                                <label for="email-input" class=" form-control-label">Name</label>
                                <input value="{{$interview->name}}" type="text"
                                       class="form-control @error('name') is-invalid @enderror" name="name"
                                       placeholder="Nhập tên đầy đủ">
                                @error('name')
                                <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="password" class=" form-control-label">Password</label>
                                <input type="password" class="form-control @error('password') is-invalid @enderror"
                                       name="password" placeholder="Nhập mật khẩu">
                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="confirm_password" class=" form-control-label">Confirm Password</label>
                                <input type="password"
                                       class="form-control @error('confirm_password') is-invalid @enderror"
                                       name="confirm_password" placeholder="Nhập Lại Mật Khẩu">
                                @error('confirm_password')
                                <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                @enderror
                            </div>


                            <div style="margin-left: 921px;background-color: white; ">
                                <button type="submit" class="btn btn-primary">Sửa</button>
                                <input type="reset" class="btn btn-default pull-right" value="Reset">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

