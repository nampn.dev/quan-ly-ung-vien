@extends('admin.layout.main')
@section('content')
    <div id="steps-wrap" class="ps ps--active-x">
        <a href="" class="step-item arrow_box">Hồ sơ ứng tuyển</a>
        <a href="" class="step-item  arrow_box">1. Lọc CV <span class="label label-danger">1</span></a>
        <a href="" class="step-item  arrow_box">2. Phỏng vấn <!----></a>
        <a href="" class="step-item  active arrow_box">3. Nhận việc <!----></a>
        <a href="" class="step-item ">4. Nam <!----></a>
        <div class="ps__rail-x" style="width: 963px; left: 0px; bottom: 0px;">
            <div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 927px;"></div>
        </div>
        <div class="ps__rail-y" style="top: 0px; right: 0px;">
            <div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 0px;"></div>
        </div>
    </div>
@endsection


