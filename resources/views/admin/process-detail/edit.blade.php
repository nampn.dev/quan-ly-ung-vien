@extends('layouts.master')
@section('title', 'Edit Work Process')
@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2 class="text-info ">{{__('Chỉnh sửa quy trình')}}</h2>
            <ol class="breadcrumb">
                <li class="text-success">
                    <a href="{{route('home')}}">{{__('Trang chủ')}}</a>
                </li>
                <li class="text-success">
                    <a href="{{ route('work-process.index') }}">{{__('Quy trình')}}</a>
                </li>
                <li class="active">
                    <strong>{{__('Cập nhật')}}</strong>
                </li>
            </ol>
        </div>
    </div>
    <div class="wrapper wrapper-content animated ecommerce">
        <div class="ibox-content m-b-sm">
            <form action="{{route('work-process.update',['work_process'=>$work_process->id])}}" method="post">
                @method('PUT')
                @csrf
                <div class="form-group" id="height-name">
                    <label for="title">{{__('Tên quy trình ')}}  <span class="column-required">*</span></label>
                    <div class="row">
                        <div class="col-sm-8">
                            <input type="text" id="input-name-work-process"
                                   class="form-control @error('name') error
	                            @enderror" name="name" value="{{old('name',$work_process->name)}}"
                                   placeholder="{{__('Nhập tên')}}">
                            <div class="error-name-wp" id="error-name-wp">
                                <span class="text text-danger"> {{showError($errors, 'name')}} </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="job_category">{{__('Chi tiết quy trình : ')}}</label>
                    <div class="row">
                        <div class="col-md-1 col-sm-1 stt-right">{{__('STT')}}</div>
                        <div class="col-md-6 col-sm-5">
                            <p>{{__('Tên bước')}}  <span class="column-required">*</span></p>
                        </div>
                        <div class="col-md-3 col-sm-4">
                            <p>{{__('Nhóm được chỉ định')}}  <span class="column-required">*</span></p>
                        </div>
                    </div>
                    @php
                        $steps = old('process_details',$work_process->processDetails);
                    @endphp
                    <div id="form-step">
                        @foreach($steps as $key=>$step)
                            <div class="row step">
                                <div class="col-md-1 col-sm-1 index-step stt-right">{{$loop->index+1}}</div>
                                <div class="col-md-6 col-sm-5">
                                    <input type="text"
                                           class="form-control step_name @if($errors->has('process_details.'.$key.'.step_name')) error
	                            @endif" maxlength="191"
                                           name="{{$loop->last?'process_details[99][step_name]':'process_details['.$key.'][step_name]'}}"
                                           value="{{old('process_details['.$key.'][step_name]',$step['step_name']) }}">
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <select
                                        class="form-control role_id @if($errors->has('process_details.'.$key.'.role_id')) error
	                            @endif"
                                        name="{{$loop->last?'process_details[99][role_id]':'process_details['.$key.'][role_id]'}}">
                                        <option value="">{{__('Chọn quyền')}}</option>
                                        @foreach($roles as $role)
                                            <option
                                                value="{{$role->id}}" {{$step['role_id']==$role->id ? 'selected="selected"':''}}>{{ Str::limit($role->name, 12)}}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="row col-md-2 col-sm-2">
                                    <div class="col-sm-6 col-md-4 col-md-offset-3">
                                        <button type="button"
                                                class="removeRow btn btn-danger glyphicon glyphicon-trash" title="{{__('Xoá')}}"></button>
                                    </div>
                                    <div class="col-sm-6 col-md-4 col-md-offset-1">
                                        <button type="button"
                                                class="btn btn-primary glyphicon glyphicon-plus addRow" title="{{__('Thêm')}}"></button>
                                    </div>
                                </div>
                                <div>
                                    <div class="col-md-6 col-md-offset-1 col-sm-5 col-sm-offset-1 error-step-name">
                                        {{showError($errors, 'process_details.'.$key.'.step_name')}}
                                    </div>
                                    <div class="col-md-3 col-sm-4 error-per">
                                        {{showError($errors, 'process_details.'.$key.'.role_id')}}
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-2 col-sm-offset-10 col-md-2 col-md-offset-10 div-save">
                        <button type="submit" class="btn btn-w-m btn-success btn-save">{{__('Lưu')}}</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
