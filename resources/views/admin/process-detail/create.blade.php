@extends('admin.layout.main')
@section('content')
    <div class="card"></div>
    <div class="wrapper wrapper-content animated ecommerce card">
        <div class="ibox-content m-b-sm">
            <form action="{{route('work-process.store')}}" method="post">
                @csrf
                <div class="form-group" style="padding: 40px;">
                    <label for="job_category">{{__('Chi tiết quy trình : ')}}</label>
                    <div class="row">
                        <div class="col-md-1 col-sm-1 stt-right">{{__('STT')}}</div>
                        <div class="col-md-6 col-sm-5">
                            <p>{{__('Tên bước')}} <span class="column-required">*</span></p>
                        </div>
                        <div class="col-md-3 col-sm-4">
                            <p>{{__('Nhóm được chỉ định')}} <span class="column-required">*</span></p>
                        </div>
                    </div>
                    @php
                        $stepsDefault = [
                            '0'=>[
                                'step_name'=>__('Check CV'),
                                'role_id'=>0
                            ],
                            '1'=>[
                                'step_name'=>__('Phone Screen'),
                                'role_id'=>0
                            ],
                            '2'=>[
                                'step_name'=>__('Interview'),
                                'role_id'=>0
                            ],
                            '3'=>[
                                'step_name'=>__('Offer'),
                                'role_id'=>0
                            ],
                            '4'=>[
                                'step_name'=>__('On-broading'),
                                'role_id'=>0
                            ]
                        ];
                    $steps = old('process_details',$stepsDefault);
                    @endphp
                    <div id="form-step">
                        @foreach($steps as $key=>$step)
                            <div class="row step">
                                <div class="col-md-1 col-sm-1 index-step stt-right">{{$loop->index+1}}</div>
                                <div class="col-md-6 col-sm-5">
                                    <input type="text"
                                           class="form-control  step_name  @if($errors->has('process_details.'.($loop->index+1).'.step_name')) error
	                            @endif" maxlength="191"
                                           name="process_details[{{$loop->index+1}}][step_name]"
                                           value="{{$step['step_name']}}">
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <select
                                        class="form-control @if($errors->has('process_details.'.($loop->index+1).'.role_id')) error
	                                    @endif  role_id " name="process_details[{{$loop->index+1}}][role_id]">
                                        <option value="">{{__('Chọn quyền')}}</option>
                                        @foreach($roles as $role)
                                            <option
                                                value="{{$role->id}}" {{$step['role_id']==$role->id ? 'selected="selected"':''}}>{{ Str::limit($role->name, 12)}}</option>
                                        @endforeach
                                    </select>
                                </div>

                            </div>
                        @endforeach
                    </div>
                    <div class="row">
                        <div class="col-sm-2 col-sm-offset-10 col-md-2 col-md-offset-10 div-save">
                            <button type="submit" class="btn btn-w-m btn-success btn-save">{{__('Lưu')}}</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
@section('script')
    <script>
        $(document).ready(function(){
            var i=1;
            $('#add').click(function(){
                i++;
                $('#dynamic_field').append('<tr id="row'+i+'">' +
                    '<td>' +
                    '<input type="text" name="name[]" placeholder="Enter your Name" class="form-control name_list" />' +
                    '</td>' +
                    '<td>' +
                    '<button type="button" class="btn btn-danger btn_remove btn-sm" name="remove" id="'+i+'"><i class="fas fa-trash"></i>Delete</button>' +
                    '<button type="button" class="ui-icon ace-icon center bigger-110 blue btn btn-info btn-sm" id="add" > <i class="fas fa-plus"></i></button>'+
                    '</td></tr>');
            });
            $(document).on('click', '.btn_remove', function(){
                var button_id = $(this).attr("id");
                $('#row'+button_id+'').remove();
            });
        });
    </script>
@endsection

