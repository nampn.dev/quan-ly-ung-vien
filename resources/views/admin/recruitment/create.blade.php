@extends('admin.layout.main')
@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid" style="padding: 11px;">
            <div class="row mb-2">
                <div class="">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{route('admin.index')}}">Home</a></li>
                        <li class="breadcrumb-item"><a href="{{route('recruitment.index')}}">List</a></li>
                        <li class="breadcrumb-item active"><a href="{{route('recruitment.create')}}">Add</a></li>
                    </ol>
                </div>
                <div class=" col-sm-12">
                    <div class="table-data__tool-right" style="position: absolute;right: 0px;top: -25px;">
                        <a href="{{route('recruitment.index')}}">
                            <button type="button" class="btn btn-info"><i class="fas fa-th-list mr-2"></i>List
                            </button>
                        </a>
                    </div>
                </div>
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->


        {{--Form Create--}}
        <div class="row" style="padding: 2px;">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <strong style="font-family: 'Open Sans', sans-serif;">Create Recruitment</strong>
                    </div>
                    <div class="card-body card-block">
                        <form action="{{route('recruitment.store')}}" method="post" enctype="multipart/form-data"
                              class="form-horizontal">
                            @csrf
                            <div class="form-group">
                                <label for="text-input" class=" form-control-label">Name</label>
                                <input value="{{old('name')}}" type="text"
                                       class="form-control @error('name') is-invalid @enderror" name="name"
                                       placeholder="Nhập tên đầy đủ">
                                @error('name')
                                <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="text-input" class=" form-control-label">Email</label>
                                <input value="{{old('email')}}" type="email"
                                       class="form-control @error('email') is-invalid @enderror"
                                       id="exampleInputEmail1" name="email" placeholder="Nhập email">
                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="text-input" class=" form-control-label">Phone</label>
                                <input value="{{old('phone')}}" type="text"
                                       class="form-control @error('phone') is-invalid @enderror"
                                       name="phone" placeholder="Nhập Số Điện Thoại">
                                @error('phone')
                                <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="text-input" class=" form-control-label">Company</label>
                                <input value="{{old('company')}}" type="text"
                                       class="form-control @error('company') is-invalid @enderror"
                                       name="company" placeholder="Nhập Tên Cty">
                                @error('company')
                                <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="text-input" class=" form-control-label">Position</label>
                                <input value="{{old('position')}}" type="text"
                                       class="form-control @error('position') is-invalid @enderror"
                                       name="position" placeholder="Nhập Vị Trí Công Tác">
                                @error('position')
                                <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="text-input" class=" form-control-label">Address</label>
                                <input value="{{old('address')}}" type="text"
                                       class="form-control @error('address') is-invalid @enderror"
                                       name="address" placeholder="Nhập Địa Chỉ">
                                @error('address')
                                <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <div class="checkbox">
                                    <label>
                                        <input class=" @error('type') is-invalid @enderror" type="checkbox"
                                               value="1" name="type"> <b>Bạn có đồng ý với thỏa thuận của chúng tôi</b>
                                        @error('type')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="password" class=" form-control-label">Password</label>
                                <input value="" type="password"
                                       class="form-control @error('password') is-invalid @enderror" name="password"
                                       placeholder="Nhập mật khẩu">
                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="confirm_password" class=" form-control-label">Confirm Password</label>
                                <input type="password"
                                       class="form-control @error('confirm_password') is-invalid @enderror"
                                       name="confirm_password"
                                       placeholder="Nhập Lại Mật Khẩu">
                                @error('confirm_password')
                                <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                @enderror
                            </div>
                            <div style="margin-left: 921px;background-color: white;">
                                <button type="submit" class="btn btn-primary">Tạo</button>
                                <input type="reset" class="btn btn-default pull-right" value="Reset">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection



