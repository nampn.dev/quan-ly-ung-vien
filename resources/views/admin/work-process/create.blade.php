@extends('admin.layout.main')
@section('content')
    <div class="card"></div>
    <div class="wrapper wrapper-content animated ecommerce card">
        <div class="ibox-content m-b-sm">
            <form action="{{route('process.candidate.store')}}" method="post">
                @csrf
                <div class="form-group" style="padding: 40px;">
                    <label for="job_category">{{__('Chi tiết quy trình : ')}}</label>
                    <div class="row">
                        <div class="col-md-1 col-sm-1 stt-right">{{__('STT')}}</div>
                        <div class="col-md-6 col-sm-5">
                            <p>{{__('Tên bước')}} <span class="column-required">*</span></p>
                        </div>
                        <div class="col-md-3 col-sm-4">
                            <p>{{__('Nhóm được chỉ định')}} <span class="column-required">*</span></p>
                        </div>
                    </div>
                    <div id="form-step">
                        @php $i = 0; @endphp
                        @foreach($workProcess as $step)
                            @php $i++; @endphp
                            <div class="row step">
                                <div class="col-md-1 col-sm-1 index-step stt-right">{{$loop->index+1}}</div>
                                <div class="col-md-6 col-sm-5">
                                    <input type="text"
                                           class="form-control  step_name" maxlength="191"
                                           name="work_process"
                                           value="{{$step->name}}">
                                    <input type="hidden" name="process[{{$i}}][work_process_id]" value="{{$step->id}}">
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <select
                                        class="form-control role_id " name="process[{{$i}}][role_id]">
                                        <option value="">Chọn quyền</option>
                                        @foreach($roles as $role)
                                            <option
                                                value="{{$role->id}}" {{$step->roleProcess->first()->role_id == $role->id ? 'selected="selected"':''}}>
                                                {{$role->name}}</option>
                                        @endforeach
                                    </select>
                                </div>

                            </div>
                        @endforeach
                    </div>
                    <div class="row">
                        <div class="col-sm-2 col-sm-offset-10 col-md-2 col-md-offset-10 div-save">
                            <button type="submit" class="btn btn-w-m btn-success btn-save">{{__('Lưu')}}</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection


