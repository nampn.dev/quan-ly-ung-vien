@extends('admin.layout.main')
@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid" style="padding: 11px;">
            <div class="row mb-2">
                <div class="">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{route('admin.index')}}">Home</a></li>
                        <li class="breadcrumb-item"><a href="{{route('candidate.index')}}">List</a></li>
                        <li class="breadcrumb-item active"><a href="">Edit</a></li>
                    </ol>
                </div>
                <div class=" col-sm-12">
                    <div class="table-data__tool-right" style="position: absolute;right: 0px;top: -25px;">
                        <a href="{{route('candidate.index')}}">
                            <button type="button" class="btn btn-info"><i class="fas fa-th-list mr-2"></i>List
                            </button>
                        </a>
                    </div>
                </div>
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->


        {{--Form Create--}}
        <div class="row" style="padding: 2px;">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <strong style="font-family: 'Open Sans', sans-serif;">Edit Candidate</strong>
                    </div>
                    <div class="card-body card-block">
                        <form action="{{route('candidate.update',['id'=>$candidate->id])}}" method="post"
                              enctype="multipart/form-data"
                              class="form-horizontal">
                            @csrf
                            <div class="form-group">
                                <label for="email-input" class=" form-control-label">Name</label>
                                <input value="{{$candidate->name}}" type="text"
                                       class="form-control @error('name') is-invalid @enderror" name="name"
                                       placeholder="Nhập tên đầy đủ">
                                @error('name')
                                <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="text-input" class=" form-control-label">Phone</label>
                                <input value="{{$candidate->phone}}" type="text"
                                       class="form-control @error('phone') is-invalid @enderror"
                                       name="phone" placeholder="Nhập Số Điện Thoại">
                                @error('phone')
                                <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="text-input" class=" form-control-label">Email</label>
                                <input value="{{$candidate->email}}" type="email"
                                       class="form-control @error('email') is-invalid @enderror"
                                       name="email" placeholder="Nhập Email">
                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="text-input" class=" form-control-label">Date Of Birth</label>
                                <input value="{{$candidate->date_of_birth}}" type="date"
                                       class="form-control @error('date_of_birth') is-invalid @enderror"
                                       name="date_of_birth" placeholder="Nhập Tên Cty">
                                @error('date_of_birth')
                                <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="text-input" class=" form-control-label">Position</label>
                                <input value="{{$candidate->position}}" type="text"
                                       class="form-control @error('position') is-invalid @enderror"
                                       name="position" placeholder="Nhập Vị Trí Công Tác">
                                @error('position')
                                <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="text-input" class=" form-control-label">Address</label>
                                <input value="{{$candidate->address}}" type="text"
                                       class="form-control @error('address') is-invalid @enderror"
                                       name="address" placeholder="Nhập Địa Chỉ">
                                @error('address')
                                <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label class=" form-control-label">CV</label>
                                <input type="file" class="form-control @error('url_cv') is-invalid @enderror"
                                       name="url_cv" placeholder="Nhập mật khẩu">
                                @error('url_cv')
                                <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                @enderror
                            </div>
                            <div style="margin-left: 921px;background-color: white; ">
                                <button type="submit" class="btn btn-primary">Sửa</button>
                                <input type="reset" class="btn btn-default pull-right" value="Reset">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

