@extends('admin.layout.main')
@section('content')
    @if (session()->has('success'))
        <div class="alert alert-success">
            @if(is_array(session('success')))
                @foreach (session('success') as $message)
                    {{ $message }}
                @endforeach
            @else
                {{ session('success') }}
            @endif
        </div>
    @endif
    <style type="text/css">
        th,td{
            vertical-align: middle !important;
        }

    </style>
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark" style="font-family: 'Open Sans', sans-serif;">Candidate</h1>
                </div>
            </div><!-- /.row -->

        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <!-- MAIN CONTENT-->
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header" style=" border-bottom: none !important;  width: 30%;">
                    <!-- Search form -->
                    <form style="margin-left: -49px;" class="form-inline d-flex justify-content-center md-form form-sm active-purple active-purple-2 mt-2">
                        <input class="form-control form-control-sm ml-3 w-75" type="text" placeholder="Search"
                               aria-label="Search" @if(Request::get('key')) value="{{Request::get('key')}}" @endif name="key">
                    </form>

                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <table class="table table-bordered" >
                        <thead>
                        <tr style="text-align: center;">
                            <th>ID</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Phone</th>
                            <th>Date </th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($candidates as $candidate)
                            <tr style="text-align: center;">
                                <td>{{$candidate->id}}</td>
                                <td>{{$candidate->name}}</td>
                                <td>{{$candidate->email}}</td>
                                <td>{{$candidate->phone}}</td>
                                <td>{{$candidate->date_of_birth}}</td>
                                <td >
                                    <div class="d-flex justify-content-center align-items-center">
                                        <a href="{{route('candidate.edit',['id'=>$candidate->id])}}"
                                           class="btn btn-info btn-sm">
                                            <i class="fas fa-pencil-alt"></i>
                                            Edit </a>
                                        {{--@endcan--}}
                                        <form action="{{route('candidate.destroy',['id'=>$candidate->id])}}"
                                              onclick="return confirm('Có muốn xóa không mà xóa?')" method="post"
                                              style="margin: 2px;">
                                            @method('DELETE')
                                            @csrf
                                            <button type="submit" class="btn btn-danger btn-sm">
                                                <i class="fas fa-trash"></i>
                                                Delete
                                            </button>
                                        </form>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <div class="d-flex" style="padding: 10px;">
                        <div class="mx-auto">
                            {{$candidates->links()}}
                        </div>
                    </div>

                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
    </div>
    <!-- END DATA TABLE -->
{{--Modal--}}
    <div class="modal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Modal title</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>Modal body text goes here.</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary">Save changes</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <style>
        .color{
            color: black;
        }
    </style>
@endsection

@section('script')
    <script>
        function showDetail(id){
            $.ajax({
                url: "{{route('api-candidate.detail')}}",
                type: 'GET',
                data:{
                    id:id
                }
            }).done(function(response) {
                html = '';
                html += ''
            });
        }
    </script>
    <script type="text/javascript">
        //TOGGLE FONT AWESOME ON CLICK
        $('.faq-links').click(function() {
            let candidateId=$(this).data("id")
            $.ajax({
                type : "GET",
                data : {
                    'id' : candidateId
                },
                url : "namlol",
                success : function (response){
                    console.log(response)
                }
            })
            return false
            $(this).find('i').toggleClass('fas fa-plus fas fa-minus')
        });
    </script>
@endsection

