@extends('admin.layout.main')
@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid" style="padding: 11px;">
            <div class="row mb-2">
                <div class="">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{route('admin.index')}}">Home</a></li>
                        <li class="breadcrumb-item"><a href="{{route('attribute.index')}}">List</a></li>
                        <li class="breadcrumb-item active"><a href="">Edit</a></li>
                    </ol>
                </div>
                <div class=" col-sm-12">
                    <div class="table-data__tool-right" style="position: absolute;right: 0px;top: -25px;">
                        <a href="{{route('attribute.index')}}">
                            <button type="button" class="btn btn-info"><i class="fas fa-th-list mr-2"></i>List
                            </button>
                        </a>
                    </div>
                </div>
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->

        {{--Form Create--}}
        <div class="row" style="padding: 2px;">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <strong style="font-family: 'Open Sans', sans-serif;">Edit Attribute</strong>
                    </div>
                    <div class="card-body card-block">
                        <form action="{{route('attribute.update',['id'=>$attribute->id])}}" method="post"
                              enctype="multipart/form-data"
                              class="form-horizontal">
                            @csrf
                            @method('PUT')
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="text-input" class=" form-control-label">Name</label>
                                        <input style="width: 87%" value="{{$attribute->name}}" type="text"
                                               class="form-control @error('name') is-invalid @enderror" name="name"
                                               placeholder="Nhập tên đầy đủ">
                                        @error('name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                    <div class="form-group ">
                                        <label for="" class=" form-control-label">Option</label>
                                        <table border="0" id="dynamic_field" class="col-lg-12 mgl "
                                               style="margin-left: -7px;">
                                            <tr class="space ">
                                                <td class="col-lg-11">
                                                    <input type="text" value="" id="option[]" name="option[]"
                                                           placeholder="Enter your option"
                                                           class="form-control option_list"/>
                                                </td>
                                                <td class="col-lg-1">
                                                    <button type="button" name="add" id="add" class="btn btn-primary ">
                                                        <i class="fa fa-plus"></i></button>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>

                            <div style="margin-left: 921px;background-color: white; ">
                                <button type="submit" class="btn btn-primary">Sửa</button>
                                <input type="reset" class="btn btn-default pull-right" value="Reset">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        CKEDITOR.replace('decription');

    </script>
    <script>
        $(document).ready(function () {
            $('.js-example-basic-multiple').select2();
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            var i = 1;
            $('#add').click(function () {
                i++;
                $('#dynamic_field').append('<tr class="space" id="row' + i + '"><td class="col-lg-11"><input type="text" name="option[]" placeholder="Enter your Option" class="form-control option_list" /></td><td class="col-lg-1"><button type="button" name="remove" id="' + i + '" class="btn btn-danger btn_remove"><i class="fa fa-times"></i></button></td></tr>');
            });
            $(document).on('click', '.btn_remove', function () {
                var button_id = $(this).attr("id");
                $('#row' + button_id + '').remove();
            });
        });
    </script>
@endsection



