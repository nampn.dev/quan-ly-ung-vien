@extends('admin.layout.main')
@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid" style="padding: 11px;">
            <div class="row mb-2">
                <div class="">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{route('admin.index')}}">Home</a></li>
                        <li class="breadcrumb-item"><a href="{{route('skill.index')}}">List</a></li>
                        <li class="breadcrumb-item active"><a href="">Edit</a></li>
                    </ol>
                </div>
                <div class=" col-sm-12">
                    <div class="table-data__tool-right" style="position: absolute;right: 0px;top: -25px;">
                        <a href="{{route('skill.index')}}">
                            <button type="button" class="btn btn-info"><i class="fas fa-th-list mr-2"></i>List
                            </button>
                        </a>
                    </div>
                </div>
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->


        {{--Form Create--}}
        <div class="row" style="padding: 2px;">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <strong style="font-family: 'Open Sans', sans-serif;">Edit Role</strong>
                    </div>
                    <div class="card-body card-block">
                        <form action="{{route('skill.update',['id'=>$skills->id])}}" method="post"
                              enctype="multipart/form-data"
                              class="form-horizontal">
                            @csrf
                            @method('PUT')
                            <div class="form-group">
                                <label for="email-input" class=" form-control-label">Name</label>
                                <input value="{{$skills->name}}" type="text"
                                       class="form-control @error('name') is-invalid @enderror" name="name"
                                       placeholder="Nhập tên đầy đủ">
                                @error('name')
                                <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                @enderror
                            </div>
                            <div style="margin-left: 921px;background-color: white; ">
                                <button type="submit" class="btn btn-primary">Sửa</button>
                                <input type="reset" class="btn btn-default pull-right" value="Reset">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

