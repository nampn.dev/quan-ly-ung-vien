<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Job extends Model
{
    protected $fillable = [
        'title',
        'status',
        'decription',
        'number',
        'address',
        'salary',
        'level'
    ];
    protected $casts = [
        'value' => 'array'
    ];
    public function skills()
    {
        return $this->morphToMany('App\Skill', 'skillable', 'skill_job_candidate');
    }
    public function attribute()
    {
        return $this->belongsToMany(Attribute::class, 'attribute_job', 'job_id', 'attribute_id')->withPivot('value');
    }
    public function candidates()
    {
        return $this->belongsToMany(Candidate::class, 'job_candidate', 'job_id', 'candidate_id');
    }

    public function jobCandidate(){
        return $this->hasMany(Job_Candidate::class);
    }
//    public function attributejob()
//    {
//        return $this->belongsToMany('App\Attribute', 'attribute_job', 'job_id', 'attribute_id');
//    }
}
