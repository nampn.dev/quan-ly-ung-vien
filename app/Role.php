<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $fillable = ['name','permissions'];

    public function users(){
        return $this->belongsToMany(User::class,'role_users','user_id','role_id');
    }
    public function permissions()
{
    return $this->belongsToMany(Permission::class, 'permission_role', 'role_id', 'permission_id');
}
    public function work_processes(){
        return $this->belongsToMany(WorkProcesses::class,'role_processes','work_process_id','role_id');
    }


//    public function hasAccess(array $permisions){
//        foreach ($permisions as $permision){
//            if ($this->hasPermission($permision)){
//                return true;
//            }
//        }
//        return false;
//    }
//    public function hasPermission(string  $permission){
//        $permission = json_decode($this->permissions,true);
//        return $permission[$permission]??false;
//    }
}
