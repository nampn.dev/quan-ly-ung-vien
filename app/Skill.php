<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Skill extends Model
{
    protected $fillable = ['name'];
    public function skills()
    {
        return $this->morphTo();
    }
    public function candidates()
    {
        return $this->morphedByMany('App\Candidate', 'skillable');
    }

    public function jobs()
    {
        return $this->morphedByMany('App\Job', 'skillable');
    }
}
