<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WorkProcesses extends Model
{
    protected $table = 'work_processes';
    protected $fillable = ['name'];

    public function roles()
    {
        return $this->belongsToMany(Role::class, 'role_processes' ,'work_process_id','role_id');
    }

    public function roleProcess(){
        return $this->hasMany(RoleProcess::class, 'work_process_id', 'id');
    }

}
