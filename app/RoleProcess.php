<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RoleProcess extends Model
{
    protected $table = 'role_processes';

    protected $fillable = ['work_process_id', 'role_id'];
}
