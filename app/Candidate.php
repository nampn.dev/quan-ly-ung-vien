<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Candidate extends Model
{
    public function skillable()
    {
        return $this->morphToMany('App\Skill', 'skillable');
    }
    protected $fillable = [
        'name',
        'phone',
        'email',
        'date_of_birth',
        'position',
        'address',
        'url_cv'
        ];
    public function jobs()
    {
        return $this->belongsToMany(Job::class,'job_candidate','candidate_id','job_id');
    }

    public function jobCandidate(){
        return $this->hasMany(Job_Candidate::class);
    }
}
