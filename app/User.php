<?php

namespace App;

use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Auth\Authenticatable as AuthenticableTrait;
use \Illuminate\Notifications\Notifiable;
class User extends Model implements Authenticatable, CanResetPasswordContract
{
    use Notifiable;
    use CanResetPassword;
    use AuthenticableTrait;
    protected $fillable = [
        'name',
        'email',
        'password',
        'phone',
        'company',
        'position',
        'address',
        'role_id',
        'type'

    ];

    public function roles(){
        return $this->belongsToMany(Role::class,'role_users', 'user_id', 'role_id');
    }
    public function role()
    {
        return $this->belongsTo(Role::class);
    }
    public function CheckPermission($check)
    {
        $roles = auth()->user()->role;
        if (!empty($roles)) {
            $permission = $roles->permissions;

            if ($permission->contains('slug', $check)) {
                return true;
            }
            return false;
        }
        else{
            $type = auth()->user()->type;
            if($type == RECRUITMENT){
                switch ($check){
                    case 'view-candidate':
                    case 'create-candidate':
                    case 'edit-candidate':
                    case 'delete-candidate':
                    case 'view-job':
                    case 'create-job':
                    case 'edit-job':
                    case 'delete-job':
                    case 'view-attribute':
                    case 'create-attribute':
                    case 'edit-attribute':
                    case 'delete-attribute':
                    case 'view-process':
                        return true;
                    default:
                        return false;
                }
            }
            elseif($type == INTERVIEW){
                switch ($check){
                    case 'view-candidate':
                    case 'create-candidate':
                    case 'edit-candidate':
                    case 'delete-candidate':
                    case 'view-job':
                    case 'create-job':
                    case 'edit-job':
                    case 'delete-job':
                        return true;
                    default:
                        return false;
                }
            }
            elseif($type == CANDIDATE){
                switch ($check){
                    case 'view-job':
                    case 'create-job':
                    case 'edit-job':
                    case 'delete-job':
                        return true;
                    default:
                        return false;
                }
            }
            else
                {
                return false;
            }
        }
    }
}
