<?php
const PER_PAGE = 5;
const ADMIN = 0;
const RECRUITMENT = 1;
const INTERVIEW = 2;
const CANDIDATE = 3;

const STEPDEFAULTS = [
        '0'=>[
            'step_name'=>'Check CV',
            'role_id'=>0,
        ],
        '1'=>[
            'step_name'=>'Phone Screen',
            'role_id'=>0
        ],
        '2'=>[
            'step_name'=>'Interview',
            'role_id'=>0
        ],
        '3'=>[
            'step_name'=>'Offer',
            'role_id'=>0
        ],
        '4'=>[
            'step_name'=>'On-broading',
            'role_id'=>0
        ],
    ];
