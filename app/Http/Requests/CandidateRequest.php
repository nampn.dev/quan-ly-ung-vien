<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CandidateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

            $id = request()->route()->parameter('id');
        return [
            'name' => 'required',
            'email' => 'required|unique:users,email,'.$id,// So sánh trên db có trùng không
            'password' => 'min:6|max:20,',
            'phone' => 'required|digits:10',
            'date_of_birth' => 'required||before:today',
            'position' => 'required', //Kiểm tra k được để trống
            'address' => 'required', //Kiểm tra k được để trống
            'confirm_password' => 'same:password,' //Nhập lại mật khẩu so với mật khẩu chính
        ];
    }
    public function messages()
    {

        return [
            'name.required'=>'Không được để trống',
            'email.required'=>'Không được để trống',
            'email.unique'=>'Email đã tồn tại',
            'phone.required'=>'Không được để trống',
            'phone.digits'=>'Số điện thoại phải là 10 số',
            'date_of_birth.required'=>'Ngày sinh k được để trống',
            'date_of_birth.before'=>'Ngày sinh k phải ngày tương lai',
//            'phone.unique'=>'Number đã tồn tại',
            'position.required'=>'Không được để trống',
            'address.required'=>'Không được để trống',
            'password.min'=>'Mật khẩu quá ngắn',
            'password.max'=>'Mật khẩu không vượt quá 20 ký tự',
            'confirm_password.same' =>' Mật khẩu nhập lại chưa chính xác',

        ];
    }
}
