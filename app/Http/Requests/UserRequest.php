<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true; //bật true để bật validate
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = request()->route()->parameter('id');
        return [
            'name' => 'required', // So sánh trên db có trùng không
            'email' => 'required|unique:users,email,' . $id,
            'password' => 'required|min:6|max:20,',
            'confirm_password' => 'same:password,' //Nhập lại mật khẩu so với mật khẩu chính
        ];
    }

    public function messages()
    {

        return [
            'name.required' => 'Không được để trống',
            'email.required' => 'Không được để trống',
            'email.unique' => 'Email đã tồn tại',
            'password.required' => 'Bạn quên chưa nhập mật khẩu',
            'password.min' => 'Mật khẩu quá ngắn. Ít nhất 6-20 ký tự',
            'password.max' => 'Mật khẩu không vượt quá 20 ký tự',
            'confirm_password.same' => ' Mật khẩu nhập lại chưa chính xác',

        ];
    }
}
