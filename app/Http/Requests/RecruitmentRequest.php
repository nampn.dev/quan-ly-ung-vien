<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RecruitmentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = request()->route()->parameter('user');
        return [
            'name' => 'required',
            'email' => 'required|unique:users,email,'.$id,// So sánh trên db có trùng không
            'password' => 'required|min:6|max:20,',
            'phone' => 'required|digits:10',
            'company' => 'required|unique:users,company,'.$id,
            'position' => 'required', //Kiểm tra k được để trống
            'address' => 'required', //Kiểm tra k được để trống
            'type' => 'required', //Kiểm tra k được để trống
            'confirm_password' => 'same:password,' //Nhập lại mật khẩu so với mật khẩu chính
        ];
    }
    public function messages()
    {

        return [
            'name.required'=>'Không được để trống',
            'email.required'=>'Không được để trống',
            'email.unique'=>'Email đã tồn tại',
            'phone.required'=>'Không được để trống',
            'phone.digits'=>'Số điện thoại phải đủ 10 số',
//            'phone.unique'=>'Number đã tồn tại',
            'company.required'=>'Không được để trống',
            'position.required'=>'Không được để trống',
            'address.required'=>'Không được để trống',
            'type.required'=>'Bạn chưa chấp thuận',
            'password.required'=>'Bạn quên chưa nhập mật khẩu',
            'password.min'=>'Mật khẩu quá ngắn',
            'password.max'=>'Mật khẩu không vượt quá 20 ký tự',
            'confirm_password.same' =>' Mật khẩu nhập lại chưa chính xác',

        ];
    }
}
