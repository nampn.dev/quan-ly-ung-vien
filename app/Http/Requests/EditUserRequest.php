<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EditUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = request()->route()->parameter('id');
        return [
            'name' => 'required',
            'password' => 'sometimes|min:6|max:20,',
            'confirm_password' => 'sometimes|same:password,' //Nhập lại mật khẩu so với mật khẩu chính
        ];
    }
    public function messages()
    {
        return [
            'name.required'=>'Không được để trống',
            'password.min'=>'Mật khẩu quá ngắn. Ít nhất 6-20 ký tự',
            'password.max'=>'Mật khẩu không vượt quá 20 ký tự',
            'confirm_password.same' =>' Mật khẩu nhập lại chưa chính xác',

        ];
    }
}
