<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class JobRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
//        $id = request()->route()->parameter('job');
        return [
            'title' => 'required',
            'level' => 'required',// So sánh trên db có trùng không
            'number' => 'required|numeric|digits:10',
            'status' => 'required',
            'address' => 'required', //Kiểm tra k được để trống
            'skill_id' => 'required', //Kiểm tra k được để trống
            'salary' => 'required',
            'decription' => 'required',
        ];
    }
    public function messages()
    {

        return [
            'title.required'=>'Không được để trống',
            'level.required'=>'Không được để trống',
            'number.numeric'=>'Số điện thoại phải là số',
            'number.required'=>'Không được để trống',
            'number.digits'=>'Số điện thoại phải là 10 số',
//            'phone.unique'=>'Number đã tồn tại',
            'status.required'=>'Không được để trống',
            'address.required'=>'Không được để trống',
            'skill_id.required'=>'Hãy chọn ít nhất một kỹ năng',
            'salary.required'=>'không được để trống',
            'decription.required'=>'không được để trống',
        ];
    }
}
