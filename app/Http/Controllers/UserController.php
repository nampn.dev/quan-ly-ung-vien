<?php

namespace App\Http\Controllers;

use App\Http\Requests\EditUserRequest;
use App\Http\Requests\UserRequest;
use App\Role;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class UserController extends Controller
{
    public function index(Request $request)
    {
        $roles = Role::all();
        $user = User::Query()->where('type', ADMIN);
        if ($request->key) {
            $search = $request->key;
            $user->where(function ($query) use ($search) {
                $query->where('name', 'like', '%' . $search . '%')->orWhere('email', 'like', '%' . $search . '%');
            });
        }
        $user = $user->paginate(PER_PAGE);
        return view('admin.user.index', [
            'user' => $user,
            'roles' => $roles,
        ]);

    }

    public function create()
    {
        $roles = Role::all();
        return view('admin.user.create', [
            'roles' => $roles,
        ]);
    }

    public function store(UserRequest $request)
    {
        $data = $request->all();
        $user = new User();
        $data['password'] = bcrypt($request->password);
        $user->fill($data)->save();
        return redirect()->route('user.index')->with('success', 'Create Success');
    }

    public function edit($id)
    {
        $roles = Role::all();
        $user = User::find($id);
//        dd($user);
        if (!$user){
            return redirect()->route('user.index');
        }
        return view('admin.user.edit', [
            'user' => $user,
            'roles' => $roles,
        ]);
    }

    public function update(EditUserRequest $request, $id)
    {
        $data = $request->all();
        $user = User::find($id);
        $data['password'] = bcrypt($request->password);
        $user->fill($data)->save();
        if (!$user){
            return redirect()->route('user.index')->with('success','Có lỗi');
        }
        return redirect()->route('user.index')->with('success', 'Edit Success');
    }

    public function destroy($id)
    {
        $user = User::find($id);
        $user->delete();
        return redirect()->route('user.index')->with('success', 'Delete Success');
    }
}
