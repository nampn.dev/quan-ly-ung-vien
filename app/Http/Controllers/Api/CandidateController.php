<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Candidate;

class CandidateController extends Controller
{
    public function show(Request $request){
        $candidate = Candidate::find($request->id);
        return response()->json($candidate);
    }

    public function namlol(Request $request){
    return response()->json(Candidate::find($request->id));
    }
}
