<?php

namespace App\Http\Controllers;

use App\Attribute;
use App\AttributeJob;
use App\Candidate;
use App\Http\Requests\JobRequest;
use App\Job;
use App\Job_Candidate;
use App\RoleProcess;
use App\Skill;
use App\User;
use App\WorkProcesses;
use Illuminate\Http\Request;

class JobController extends Controller
{
    public function index(Request $request)
    {
        $attributes = Attribute::all();
        $jobs = Job::Query();
        $workprocess = WorkProcesses::all();
        //search
        if ($request->key) {
            $search = $request->key;
            $jobs->where(function ($query) use ($search) {
                $query->where('title', 'like', '%' . $search . '%')
                    ->orWhere('number', 'like', '%' . $search . '%')
                    ->orWhere('address', 'like', '%' . $search . '%')
                    ->orWhere('salary', 'like', '%' . $search . '%')
                    ->orWhere('level', 'like', '%' . $search . '%');
            });
        }
        $jobs = $jobs->paginate(PER_PAGE);
        return view('admin.job.index', [
            'jobs' => $jobs,
            'attributes' => $attributes,
            'filter' =>[],
            'workprocess' => $workprocess
        ]);
    }

    public function create()
    {
        $attributes = Attribute::all();
        $skills = Skill::all();
        return view('admin.job.create', [
            'skills' => $skills,
            'attributes' => $attributes,
        ]);
    }

    public function store(JobRequest $request)
    {
        $job = new Job();
        $data = $request->all();
        $job->fill($data)->save();
        foreach ($request->attribute as $key => $value) {
            $value = json_encode($value);
            $attribute[$key] = ['value' => $value];
            $job->attribute()->sync($attribute);
        }
        $job->skills()->sync($data['skill_id']);
        return redirect()->route('job.index')->with('success', 'Create Success');
    }

    public function edit($id)
    {
        $skills = Skill::all();
        $attributes = Attribute::all();
        $jobs = Job::findorFail($id);
        return view('admin.job.edit', [
            'jobs' => $jobs,
            'attribute' => $attributes,
            'skills' =>$skills
        ]);
    }

    public function update(JobRequest $request, $id)
    {
        $data = $request->all();
        $jobs = Job::find($id);
        $jobs->fill($data)->save();
        return redirect()->route('job.index')->with('success', 'Edit Success');
    }

    public function destroy($id)
    {
        $job = Job::find($id);
        if(!$job->delete()){
            return back()->with('error', 'Something went wrong, please try again!');
        }
        return back()->with('success', 'Delete Job successfully!');
    }
    public function attribute(Request $request){
        $attribute = Attribute::find($request->attribute_id);
        $option = $attribute->option;
        return response()->json($option);
    }
    //job_candidate
    public function job_index($id)
    {
        $a = Job_Candidate::all();
        $job = Job::find($id);
        $workProcess = WorkProcesses::all();
        return view('admin.job.job_candidate.index', [
            'job' => $job,
            'a' => $a,
            'workProcess' => $workProcess
        ]);
    }
    public function job_list($id)
    {
        $jobCandidate = Job_Candidate::where('job_id', $id)->pluck('candidate_id'); //
        $candidates = Candidate::whereNotIn('id', $jobCandidate)->paginate(PER_PAGE); //
//        dd($candidates);
        return view('admin.job.job_candidate.list', [
            'candidates' => $candidates,
            'id'=>$id
        ]);
    }
    public function addJobCandidate(Request $request, $id){
        $job = Job::find($id);
        foreach ($request->id as $candidateId){
            $job->jobCandidate()->create(['candidate_id' => $candidateId, 'status' => 1]);
        }
        return redirect()->route('job.job_candidate.index', ['id' => $id])->with('success', 'Thêm ứng viên vào công việc thành công');
    }
    public function job_create($id)
    {
        return view('admin.job.job_candidate.create', compact('id'));
    }
    public function job_store(Request $request)
    {
        $data = $request->all();
        $candidate = new Candidate();
        $status = 1;
        $candidate->fill($data)->save();
        $candidate->jobs()->sync(
            [
                $data['jobId']=>[
                    'status'=>$status
                ]
            ]
        );
        return redirect()->route('job.job_candidate.index', ['id'=>$data['jobId']])->with('success', 'Create Success');
    }
    public function changeCandidate(Request $request){
        return response()->json(Candidate::find($request->id));
    }
    public function rateCandidate(Request $request){
        foreach ($request->process as $process){
            $jobCandidate = Job_Candidate::where('job_id', $request->job_id)->where('candidate_id', $request->candidate_id);
            if($process['status'] == 3){
                if($process['work_process_id'] == 5){
                    $jobCandidate->update(['status' => $process['status']]);
                }else{
                    $jobCandidate->update(['status' => 1]);
                }
            }else{
                $jobCandidate->update(['status' => $process['status']]);
            }

        }
        return back();
    }
}
