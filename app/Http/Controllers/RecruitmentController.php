<?php

namespace App\Http\Controllers;

use App\Http\Requests\EditRecruitmentRequest;
use App\Http\Requests\RecruitmentRequest;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class RecruitmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $recruitments = User::Query()->where('type', RECRUITMENT);
        if ($request->key) {
            $search = $request->key;
            $recruitments->where(function ($query) use ($search) {
                $query->where('name', 'like', '%' . $search . '%')
                    ->orWhere('email', 'like', '%' . $search . '%')
                    ->orWhere('phone', 'like', '%' . $search . '%')
                    ->orWhere('company', 'like', '%' . $search . '%')
                    ->orWhere('position', 'like', '%' . $search . '%')
                    ->orWhere('address', 'like', '%' . $search . '%');
            });
        }
        $recruitments = $recruitments->paginate(PER_PAGE);
        return view('admin.recruitment.index', [
            'recruitments' => $recruitments,
        ]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.recruitment.create');
    }

    public function store(RecruitmentRequest $request)
    {
        $data = $request->all();
        $recruitments = new User();
        $data['password'] = bcrypt($request->password);
        $type = 0;
        if ($request->has('type')) {
            $type = $request->input('type');
        }
        $recruitments->type = $type;
        $recruitments->fill($data)->save();
        return redirect()->route('recruitment.index')->with('success', 'Create Success');
    }

    public function edit($id)
    {
        $recruitment = User::findorFail($id);
        return view('admin.recruitment.edit', [
            'recruitment' => $recruitment,
        ]);
    }

    public function update(EditRecruitmentRequest $request, $id)
    {
        $data = $request->all();
        $recruitment = User::find($id);
        $data['password'] = bcrypt($request->password);
        $recruitment->fill($data)->save();
        return redirect()->route('recruitment.index')->with('success', 'Edit Success');
    }

    public function destroy($id)
    {
        $recruitment = User::find($id);
        $recruitment->delete();
        return redirect()->route('recruitment.index')->with('success', 'Delete Success');
    }
}
