<?php

namespace App\Http\Controllers;

use App\Candidate;
use App\Job;
use App\Skill;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class SkillController extends Controller
{
    public function index(Request $request)
    {

        $skills = Skill::Query();
        if ($request->key) {
            $search = $request->key;
            $skills->where(function ($query) use ($search) {
                $query->where('name', 'like', '%' . $search . '%')->orWhere('email', 'like', '%' . $search . '%');
            });
        }
        $skills = $skills->paginate(PER_PAGE);
        return view('admin.skill.index',[
            'skills' => $skills,
        ]);
    }
    public function create()
    {
        return view('admin.skill.create');
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $skills = new Skill();
        $skills->fill($data)->save();
        return redirect()->route('skill.index')->with('success', 'Create Success');
    }

    public function edit($id)
    {
        $skills = Skill::findorFail($id);
        return view('admin.skill.edit', [
            'skills' => $skills,
        ]);
    }

    public function update(Request $request, $id)
    {
        $data = $request->all();
        $skills = Skill::find($id);
        $skills->fill($data)->save();
        return redirect()->route('skill.index')->with('success', 'Edit Success');
    }

    public function destroy($id)
    {

       ;
    }
}
