<?php

namespace App\Http\Controllers;

use App\Candidate;
use App\Http\Requests\CandidateRequest;
use Illuminate\Http\Request;

class CandidateController extends Controller
{
    public function index(Request $request)
    {
        $candidates = Candidate::Query();
        if ($request->key) {
            $search = $request->key;
            $candidates->where(function ($query) use ($search) {
                $query->where('name', 'like', '%' . $search . '%')
                    ->orWhere('email', 'like', '%' . $search . '%')
                    ->orWhere('phone', 'like', '%' . $search . '%')
                    ->orWhere('date_of_birth', 'like', '%' . $search . '%')
                    ->orWhere('position', 'like', '%' . $search . '%')
                    ->orWhere('address', 'like', '%' . $search . '%');
            });
        }
        $candidates = $candidates->paginate(PER_PAGE);
        return view('admin.candidate.index', [
            'candidates' => $candidates,
        ]);
    }
    public function create()
    {
        return view('admin.candidate.create');
    }

    public function store(CandidateRequest $request)
    {
        $data = $request->all();
        $candidates = new Candidate();
        $candidates->fill($data)->save();
        return redirect()->route('candidate.index')->with('success', 'Create Success');
    }

    public function edit($id)
    {
        $candidate = Candidate::findorFail($id);
        return view('admin.candidate.edit', [
            'candidate' => $candidate,
        ]);
    }
    public function update(CandidateRequest $request, $id)
    {
        $data = $request->all();
        $candidate = Candidate::find($id);
        $candidate->fill($data)->save();
        return redirect()->route('candidate.index')->with('success', 'Edit Success');
    }
    public function destroy($id)
    {
        $candidate = Candidate::find($id);
        $candidate->delete();
        return redirect()->route('candidate.index')->with('success', 'Delete Success');
    }


}
