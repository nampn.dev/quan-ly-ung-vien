<?php

namespace App\Http\Controllers;

use App\Http\Requests\EditUserRequest;
use App\Http\Requests\UserRequest;
use App\Role;
use App\User;
use Illuminate\Http\Request;

class InterviewController extends Controller
{
    public function index(Request $request)
    {
        $roles = Role::all();
        $interviews = User::Query()->where('type', INTERVIEW);
        if ($request->key) {
            $search = $request->key;
            $interviews->where(function ($query) use ($search) {
                $query->where('name', 'like', '%' . $search . '%')->orWhere('email', 'like', '%' . $search . '%');
            });
        }
        $interviews = $interviews->paginate(PER_PAGE);
        return view('admin.interview.index', [
            'interviews' => $interviews,
            'roles' => $roles,

        ]);
    }

    public function create()
    {
        $roles = Role::all();
        return view('admin.interview.create',[
            'roles' =>$roles,
        ]);
    }

    public function store(UserRequest  $request)
    {
        $data = $request->all();
        $user = new User();
        $data['password'] = bcrypt($request->password);
        $user->fill($data)->save();
//        $user->roles()->sync($request->role);
        return redirect()->route('interview.index')->with('success', 'Create Success');
    }
    public function edit($id)
    {
        $roles = Role::all();
        $interview = User::findorFail($id);
        return view('admin.interview.edit', [
            'interview' => $interview,
            'roles' => $roles,
        ]);
    }

    public function update(EditUserRequest $request, $id)
    {
        $data = $request->all();

        $interview = User::find($id);
        $data['password'] = bcrypt($request->password);
        $interview->fill($data)->save();
        return redirect()->route('interview.index')->with('success', 'Edit Success');
    }

    public function destroy($id)
    {
        $interview = User::find($id);
        $interview->delete();
        return redirect()->route('interview.index')->with('success', 'Delete Success');
    }
}
