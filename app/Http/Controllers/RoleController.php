<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Role;
use App\Permission;
use App\RolePermission;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
class RoleController extends Controller
{
//    public function __construct()
//    {
//        $this->middleware('can:view-role')->only('index');
//        $this->middleware('can:create-role')->only('create');
//        $this->middleware('can:edit-role')->only('edit');
//        $this->middleware('can:delete-role')->only('destroy');
//    }

    public function index(Request $request)
    {
        $roles = Role::query();
        if ($request->key) {
            $search = $request->key;
            $roles->where(function ($query) use ($search) {
                $query->where('name', 'like', '%' . $search . '%');
            });
        }
        $roles = $roles->paginate(PER_PAGE);
        return view('admin.role.index',compact('roles'));
    }

    public function create()
    {
        $permissions = Permission::all();
        return view('admin.role.create', compact('permissions'));

    }

    public function store(Request $request)
    {
        $request->validate([
            'name'=> 'required|unique:roles,name',
        ]);
        $role = new Role;
        $role->name = $request->name;
        if($role->save()){
            $role->permissions()->sync($request->permission);
            return redirect()->route('role.index')->with('success', 'Add role successfully!');
        }else{
            return back()->with('error', 'Something went wrong, please try again');
        }

    }
    public function edit($id)
    {
        $role = Role::find($id);
        $permissions = Permission::all();
        return view('admin.role.edit', compact('role', 'permissions'));
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required|unique:roles,name,'.$id,
        ]);
        $role = Role::find($id);
        $role->name = $request->name;
        $role->save();
        $role->permissions()->sync($request->permission);
        return redirect()->route('role.index')->with('success', 'Edit role successfully!');
    }
    public function destroy($id)
    {
       try {
            DB::beginTransaction();
            DB::table('roles')->delete($id);
            DB::table('users')->where('role_id', $id)->update(['role_id' => null]);
            DB::commit();
       } catch (\Exception $e) {
            DB::rollBack();
            Log::error($e->getMessage());
            return back()->with('error', 'Something went wrong, please try again');
       }
       return redirect()->route('role.index')->with('success', 'Delete role successfully!');

    }
}
