<?php

namespace App\Http\Controllers;

use App\Candidate;
use App\Job_Candidate;
use App\Role;
use App\RoleProcess;
use App\WorkProcesses;
use Illuminate\Http\Request;

class WorkProcessController extends Controller
{

    public function create()
    {
        $workProcess = WorkProcesses::all();
        $roles = Role::take(3)->get();
        return view('admin.work-process.create',compact('roles', 'workProcess'));
    }

    public function store(Request $request)
    {
        $data = $request->all();
        try {
            foreach ($request->process as $process){
                $workProcess = WorkProcesses::find($process['work_process_id']);
                $workProcess->roleProcess()->update(['role_id'=>$process['role_id']]);
            }
        } catch (\Exception $e) {
            return redirect()->route('process.candidate.create')
                ->with('error', __('Có lỗi xảy ra, vui lòng thử lại sau!'));
        }
        return redirect()->route('process.candidate.create')
            ->with('success', __('Phân quyền quy trình thành công.'));
    }


}
