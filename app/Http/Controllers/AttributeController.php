<?php

namespace App\Http\Controllers;

use App\Attribute;
use App\Http\Requests\AttributeRequest;
use Illuminate\Http\Request;

class AttributeController extends Controller
{
    public function index(Request $request)
    {
        $attributes = Attribute::Query();
        if ($request->key) {
            $search = $request->key;
            $attributes->where(function ($query) use ($search) {
                $query->where('name', 'like', '%' . $search . '%')->orWhere('option', 'like', '%' . $search . '%');
            });
        }
        $attributes = $attributes->paginate(PER_PAGE);
        return view('admin.attribute.index', compact('attributes'));
    }

    public function create()
    {
        return view('admin.attribute.create');
    }

    public function store(AttributeRequest $request)
    {
        $attribute = new Attribute;
        $data = $request->all();
        $attribute->fill($data)->save();
        return redirect()->route('attribute.index')->with('success', 'Create attribute successfully!');
    }

    public function edit($id)
    {
        if (!$attribute = Attribute::find($id)) {
            return back()->with('Fail');
        }
        return view('admin.attribute.edit', compact('attribute'));
    }

    public function update(Request $request, $id)
    {
        $attribute = Attribute::find($id);
        $data = $request->all();
        $attribute->fill($data)->save();
        if (!$attribute->save()) {
            return back()->with('error', 'Some thing went wrong, please try again!');
        }
        return redirect()->route('attribute.index')->with('success', 'Update attribute successfully!');

    }

    public function destroy($id)
    {
        $attribute = Attribute::find($id);
        if (!$attribute->delete()) {
            return back()->with('error', 'Some thing went wrong, please try again!');
        }
        return redirect()->route('attribute.index')->with('success', 'Delete attribute successfully!');
    }


}
