<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Attribute extends Model
{
    protected $casts = [
        'option' => 'array',
    ];
    protected $fillable = [
      'name',
      'option',
    ];
    public function jobs()
    {
        return $this->belongsToMany(Job::class,'attribute_job','attribute_id','job_id');
    }
}
