<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use phpDocumentor\Reflection\Types\False_;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        /*View*/
        Gate::define('view-recruitment',function ($user){
            return $user->checkPermission('view-recruitment');
        });
        Gate::define('view-user',function ($user){
            return $user->checkPermission('view-user');
        });
        Gate::define('view-interview',function ($user){
            return $user->checkPermission('view-interview');
        });
        Gate::define('view-job',function ($user){
            return $user->checkPermission('view-job');
        });
        Gate::define('view-skill',function ($user){
            return $user->checkPermission('view-skill');
        });
        Gate::define('view-attribute',function ($user){
            return $user->checkPermission('view-attribute');
        });
        Gate::define('view-role',function ($user){
            return $user->checkPermission('view-role');
        });
        Gate::define('view-candidate',function ($user){
            return $user->checkPermission('view-candidate');
        });
        //Create
        Gate::define('create-user',function ($user){
            return $user->checkPermission('create-user');
        });
        Gate::define('create-recruitment',function ($user){
            return $user->checkPermission('create-recruitment');
        });
        Gate::define('create-interview',function ($user){
            return $user->checkPermission('create-interview');
        });
        Gate::define('create-job',function ($user){
            return $user->checkPermission('create-job');
        });
        Gate::define('create-skill',function ($user){
            return $user->checkPermission('create-skill');
        });
        Gate::define('create-attribute',function ($user){
            return $user->checkPermission('create-attribute');
        });
        Gate::define('create-role',function ($user){
            return $user->checkPermission('create-role');
        });
        Gate::define('create-candidate',function ($user){
            return $user->checkPermission('create-candidate');
        });
        //Edit
        Gate::define('edit-recruitment',function ($user){
            return $user->checkPermission('edit-recruitment');
        });
        Gate::define('edit-interview',function ($user){
            return $user->checkPermission('edit-interview');
        });
        Gate::define('edit-user',function ($user){
            return $user->checkPermission('edit-user');
        });
        Gate::define('edit-job',function ($user){
            return $user->checkPermission('edit-job');
        });
        Gate::define('edit-skill',function ($user){
            return $user->checkPermission('edit-skill');
        });
        Gate::define('edit-attribute',function ($user){
            return $user->checkPermission('edit-attribute');
        });
        Gate::define('edit-role',function ($user){
            return $user->checkPermission('edit-role');
        });
        Gate::define('edit-candidate',function ($user){
            return $user->checkPermission('edit-candidate');
        });
        //Delete
        Gate::define('delete-recruitment',function ($user){
            return $user->checkPermission('delete-recruitment');
        });
        Gate::define('delete-interview',function ($user){
            return $user->checkPermission('delete-interview');
        });
        Gate::define('delete-user',function ($user){
            return $user->checkPermission('delete-user');
        });
        Gate::define('delete-job',function ($user){
            return $user->checkPermission('delete-job');
        });
        Gate::define('delete-skill',function ($user){
            return $user->checkPermission('delete-skill');
        });
        Gate::define('delete-attribute',function ($user){
            return $user->checkPermission('delete-attribute');
        });
        Gate::define('delete-role',function ($user){
            return $user->checkPermission('delete-role');
        });
        Gate::define('delete-candidate',function ($user){
            return $user->checkPermission('delete-candidate');
        });
    }
}
