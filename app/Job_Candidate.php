<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Job_Candidate extends Model
{
    protected $table = 'job_candidate';

    protected $fillable = ['candidate_id', 'job_id', 'status'];

}
